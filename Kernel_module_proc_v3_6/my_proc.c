/*Создание файлов в каталоге my_proc_kernel_v3_6 в файловой системе /proc/
my_proc_kernel_v3_6/jiffies - количество тиков таймера
my_proc_kernel_v3_6/jiffies2 - ссылка на my_proc_kernel_v3_6/jiffies
my_proc_kernel_v3_6/seconds - время (в секундах) работы системы
my_proc_kernel_v3_6/my_message - вывод и запись сообщения
my_proc_kernel_v3_6/success - поздравления*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/jiffies.h>
#include <asm/uaccess.h>

#define MYKERNEL_ENTRY "my_proc_kernel_v3_6" //имя каталога

#define MSG_LEN 100 //маскимальная длина сообщения

/*структура данных для my_message*/
struct msg_data_t
{
    char value[MSG_LEN + 1];
};

static struct proc_dir_entry
    *example_dir, *jiffies_file, *symlink, *seconds_file, *msg_file, *success_file;

struct msg_data_t msg_data;

static int proc_read_jiffies
(char *page, char **start, off_t off, int count, int *eof, void *data)
{
    int len;

    len = sprintf(page, "Your processor timer ticked %lu times\n", jiffies);

    return len;
}

static int proc_read_seconds
(char *page, char **start, off_t off, int count, int *eof, void *data)
{
    int len;
    unsigned long sec;

    sec = jiffies / HZ;

    len = sprintf(page, "Your Linux system works %lu seconds\n", sec);

    return len;
}

static int proc_read_msg
(char *page, char **start, off_t off, int count, int *eof, void *data)
{
    int len;
    struct msg_data_t *msg_data = (struct msg_data_t *)data;

    len = sprintf(page, "Last message: %s", msg_data->value);

    return len;
}

static int proc_write_msg
(struct file *file, const char *buffer, unsigned long count, void *data)
{
    int len;
    struct msg_data_t *msg_data = (struct msg_data_t *)data;

    if(count > MSG_LEN)
        len = MSG_LEN;
    else
        len = count;

    if(copy_from_user(msg_data->value, buffer, len))
        return -EFAULT;

    msg_data->value[len] = '\0';

    return len;
}

static int proc_read_success
(char *page, char **start, off_t off, int count, int *eof, void *data)
{
    int len;

    len = sprintf(page, "Congratulations!\nYour Linux experiment successed!\n");

    return len;
}

static int __init procfiles_init(void)
{
    int rv = 0;

    //создание каталога /proc/mykernel_test
    example_dir = proc_mkdir(MYKERNEL_ENTRY, NULL);
    if (example_dir == NULL)
    {
        rv = -ENOMEM;
        goto out;
    }
    //example_dir->owner = THIS_MODULE; //в версии 3.6 поле owner в структуре proc_dir_entry отсутствует

    //создание файла jiffies
    jiffies_file =
        create_proc_read_entry("jiffies", 0444, example_dir, proc_read_jiffies, NULL);
    if (jiffies_file == NULL)
    {
        rv  = -ENOMEM;
        goto no_jiffies;
    }
    //jiffies_file->owner = THIS_MODULE;

    //создание симлинка на jiffies
    symlink = proc_symlink("jiffies2", example_dir, "jiffies");
    if (symlink == NULL)
    {
        rv = -ENOMEM;
        goto no_symlink;
    }
    //symlink->owner = THIS_MODULE;

    //создание файла seconds
    seconds_file =
        create_proc_read_entry("seconds", 0444, example_dir, proc_read_seconds, NULL);
    if (seconds_file == NULL)
    {
        rv  = -ENOMEM;
        goto no_seconds;
    }
    //seconds_file->owner = THIS_MODULE;

    //создание файлов msg
    msg_file = create_proc_entry("my_message", 0644, example_dir);
    if (msg_file == NULL)
    {
        rv = -ENOMEM;
        goto no_msg;
    }
    //изначально в сообщении записывается "#echo "your message > /proc/my_proc_kernel_v3_6/my_message" "
    strcpy(msg_data.value, "put in root mode: # echo <your message> > /proc/my_proc_kernel_v3_6/my_message\n");
    msg_file->data = &msg_data;
    msg_file->read_proc = proc_read_msg;
    msg_file->write_proc = proc_write_msg;
    //msg_file->owner = THIS_MODULE;

    //создание файла success
    success_file =
        create_proc_read_entry("success", 0444, example_dir, proc_read_success, NULL);
    if (success_file == NULL)
    {
        rv = -ENOMEM;
        goto no_success;
    }
    //success_file->owner = THIS_MODULE;

    printk(KERN_INFO "/proc/%s created\n", MYKERNEL_ENTRY);
    return 0;

no_success:
    remove_proc_entry("my_message", example_dir);
no_msg:
    remove_proc_entry("seconds", example_dir);
no_seconds:
    remove_proc_entry("jiffies2", example_dir);
no_symlink:
    remove_proc_entry("jiffies", example_dir);
no_jiffies:
    remove_proc_entry(MYKERNEL_ENTRY, NULL);
out:
    return rv;
}


static void __exit procfiles_exit(void)
{
    remove_proc_entry("success", example_dir);
    remove_proc_entry("my_message", example_dir);
    remove_proc_entry("seconds", example_dir);
    remove_proc_entry("jiffies2", example_dir);
    remove_proc_entry("jiffies", example_dir);
    remove_proc_entry(MYKERNEL_ENTRY, NULL);

    printk(KERN_INFO "/proc/%s removed\n", MYKERNEL_ENTRY);
}


module_init(procfiles_init);
module_exit(procfiles_exit);

MODULE_LICENSE("GPL");
