/*
Заменить заданные символы на пробелы
Параметры командной строки:
1. Имя входного файла
2. Заданный символ
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_BUFFER_SIZE 1024

int main(int argc, char** argv)
{
    FILE* file;
    FILE* temp;
    char ch;

    if(argc < 3)
    {
        fprintf(stderr, "Мало аргументов. Используйте <имя входного файла> <заданный символ>\n");
        exit(1);
    }

    if((file = fopen(argv[1], "r")) == NULL)
    {
        printf("Невозможно открыть файл.\n");
        exit(1);
    }
    if((temp = fopen("temp_file", "w")) == NULL)
    {
        printf("Невозможно открыть файл.\n");
        exit(1);
    }

    while((ch = fgetc(file)) != EOF)
    {
        if(ch == argv[2][0])
        {
            ch = ' ';
        }
        fputc(ch, temp);
    }   
    
    fclose(temp);
    fclose(file);
    
    remove(argv[1]);
    rename("temp_file",argv[1]);

    return 0;
}
