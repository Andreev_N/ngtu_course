/*Исключить строки, содержащие хотя бы один заданный символ 
Параметры командной строки:
1. Имя входного файла 
2. Заданный символ
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGHT 50
#define MAX_BUFFER_SIZE 1024

void create_new_file_name(char* old_name, char* new_name)
{
    char* tochka;
    tochka = strchr(old_name, '.'); // поиск точки в имени
    if(!tochka)
    {
        strcpy(new_name, old_name);
        strcat(new_name, ".new\0");
    }
    else
    {
        strncpy(new_name, old_name, tochka - old_name); //копирование до точки
        strcat(new_name, ".new\0");
    }
}

int main(int argc, char** argv)
{
    FILE* f_old;
    FILE* f_new;
    char newname[MAX_NAME_LENGHT];
    char buffer[MAX_BUFFER_SIZE];

    if(argc < 3)
    {
        fprintf(stderr, "Мало аргументов. Используйте <имя файла> <символ, с которым пропускать строки>\n");
        exit(1);
    }

    create_new_file_name(argv[1], newname);

    if((f_old = fopen(argv[1], "r")) == NULL)
    {
        printf("Невозможно открыть файл.\n");
        exit(1);
    }

    if((f_new = fopen(newname, "w")) == NULL)
    {
        printf("Невозможно открыть файл.\n");
        exit(1);
    }

    while(fgets(buffer, MAX_BUFFER_SIZE, f_old) != NULL)
    {
        if(strchr(buffer, argv[2][0]) == NULL)
        {
            fputs(buffer, f_new);            
        }
    }    

    fclose(f_old);
    fclose(f_new);

    return 0;
}
