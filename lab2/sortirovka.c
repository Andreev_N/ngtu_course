#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 1024 /* максимальная длина строки */

int inp_str(char* string, int maxlen)
{
    fgets(string, maxlen, stdin);
    string[strlen(string) - 1] = '\0'; //убираем \n в конце строки
    return strlen(string);
}

//ручная сортировка
void my_sort(char* string[], int* mas_size, int* exit_data)
{
    int N = 0; // количество перестановок
    char* temp;
    for(int i = 0; i < *mas_size; i++)
    {
        for(int j = *mas_size - 1; j > i; j--)
        {
            if(strlen(string[j]) < strlen(string[j - 1]))
            {
                temp = string[j];
                string[j] = string[j - 1];
                string[j - 1] = temp;
                N++;
            }
        }
    }
    exit_data[0] = N;
    exit_data[1] = strlen(string[0]);
}

/* //для сортировки через встроенную qsort
static int cmpstringp(const void* p1, const void* p2)
{
    return strlen(*(char* const*)p1) - strlen(*(char* const*)p2);
}

void sort_by_qsort(char* string[], int mas_size, int* exit_data)
{
    qsort(string, mas_size, sizeof(char*), cmpstringp);
    //exit_data[0] = N;
    exit_data[1] = strlen(string[0]);
}
*/

void out_str(char* string)
{
    printf("%s\n", string);
}

void freeMas(char** mas, int *count)
{
    for(int i = 0; i < *count; i++)
    {
        free(mas[i]); // освобождаем память для отдельной строки
    }
    free(mas); // освобождаем памать для массива указателей на строки
}

int main(int argc, char** argv)
{
    int ex_mas[2]; //массив для записи требуемых выходных параметров(длины наименьшей строки и количества перестановок)
    char** mas = NULL;    //указатель на массив указателей на строки
    char buffer[MAX_LEN]; //буфер для записи строк
    int count;            // число строк
    int len;              //длина очередной строки

    memset(buffer, '\0', MAX_LEN); // очищаем буфер

    printf("Введите количество строк:\n");
    scanf("%d", &count);
    mas = (char**)malloc(sizeof(char*) * count); // выделяем память для массива указателей

    printf("Введите строки:\n");

    getchar(); //убираем \n из входного потока

    for(int i = 0; i < count; i++)
    {
        len = inp_str(buffer, MAX_LEN);
        mas[i] = (char*)malloc(sizeof(char) * len); //выделяем память для строки
        strcpy(mas[i], buffer); //копируем строку из буфера в массив указателей
    }

    my_sort(mas, &count, ex_mas);

    printf("Строки после сортировки:\n");
    for(int i = 0; i < count; i++)
    {
        out_str(mas[i]);
    }
    printf("Число перестановок=%d\nМинимальный размер строки=%d\n", ex_mas[0], ex_mas[1]);
    freeMas(mas, &count);

    return 0;
}
