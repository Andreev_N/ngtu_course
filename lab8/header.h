#ifndef __HEADER_H__
#define __HEADER_H__

#include <stdlib.h>
#include <string.h>
#include <sys/errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MSGPERM 0600   // права доступа
#define MSGKEY 32769 //ключ для очереди
#define TYPE_FOR_SERVER 1 //тип для отправки запросов серверу в очередь
#define TYPE_FOR_CLIENT 2 //тип для приема ответов сервера в очереди
#define MAXFILENAME 64 // максимальная длина имени файла
#define MAX_SEND_SIZE 80 // максимальный размер сообщения в очереди

struct mymsg_buf
{
    long mtype;
    long int c_sum;
    char filename[MAXFILENAME];
    
};

void send_message(int qid, struct mymsg_buf* qbuf, long type, char* text, long int sum);

void read_message(int qid, struct mymsg_buf* qbuf, long type);

#endif
