#include <stdio.h>
#include <locale.h>
#include <wchar.h>//для считывания русских символов через fgetwc

#include "header.h"

int main(int argc, char **argv)
{    
    FILE *file;
    wchar_t ch; //уширенный char для русских символов
    long int sum=0; //контрольная сумма
    struct mymsg_buf qbuf;
    int msgqid;
    setlocale(LC_ALL,"");

    /*подключаемся к очереди*/
    msgqid = msgget(MSGKEY, MSGPERM | IPC_EXCL);
    if(msgqid < 0)
    {
        perror(strerror(errno));
        printf("Child can't connect to queue\n");
        exit(1);
    }
    
    //читаем сообщение из очереди с типом TYPE_FOR_SERVER
    read_message(msgqid, &qbuf, TYPE_FOR_SERVER);

    if((file = fopen(qbuf.filename, "r")) == NULL)
    {
        printf("Child can't open file %s\n",qbuf.filename);
        //отправляем сообщение в очередь с типом TYPE_FOR_CLIENT и суммой равной 0
        send_message(msgqid, (struct mymsg_buf*)&qbuf, TYPE_FOR_CLIENT, qbuf.filename,0);
        exit(2);
    }

    //подсчет контрольной суммы
    while((ch = fgetwc(file)) != EOF)
    { 
        sum+=ch;
    }    
    
    fclose(file);
    
    //отправляем сообщение с подсчитанной контрольной суммой и с типом TYPE_FOR_CLIENT
    send_message(msgqid, (struct mymsg_buf*)&qbuf, TYPE_FOR_CLIENT, qbuf.filename,sum);

  
  return 0;
}
