#include <stdio.h>

#include "header.h"

void send_message(int qid, struct mymsg_buf* qbuf, long type, char* text, long int sum)
{
    qbuf->mtype = type;
    strcpy(qbuf->filename, text);
    qbuf->c_sum=sum;
    
    if((msgsnd(qid, (struct msgbuf*)qbuf, sizeof(long int)+strlen(qbuf->filename) + 1, 0)) == -1)
    {
        perror("msgsnd");
        exit(1);
    }
}

void read_message(int qid, struct mymsg_buf* qbuf, long type)
{
    qbuf->mtype = type;
    msgrcv(qid, (struct msgbuf*)qbuf, MAX_SEND_SIZE, type, 0);
}
