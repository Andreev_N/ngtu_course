#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

#include "header.h"

int main(int argc, char* argv[])
{
    int pid[argc];    //для массива PIDов
    struct mymsg_buf qbuf;
    int msgqid, rc;

    if(argc < 2)
    {
        printf("Usage: ./main file file ...\n");
        exit(-1);
    }

    /*создание очереди*/
    msgqid = msgget(MSGKEY, MSGPERM | IPC_CREAT | IPC_EXCL);
    if(msgqid < 0)
    {
        perror(strerror(errno));
        printf("failed to create message queue\n");
        exit(1);
    }
    else
        printf("message queue %d created\n", msgqid);

    for(int i = 1; i < argc; i++)
    {
        pid[i] = fork();
        if(-1 == pid[i])
        {
            perror("fork"); /* произошла ошибка */
            exit(2);        /*выход из родительского процесса*/
        }
        else if(pid[i] == 0)
        { /*Ребенок (сервер)*/
            if(execl("./server", "server", NULL) < 0)
            {
                printf("ERROR while start processing file %d\n", i);
                exit(3);
            }
        }
    }

    /*Родитель (клиент)*/
    for(int i = 1; i < argc; i++)
    {
        //посылаем в очередь сообщение с типом 1(TYPE_FOR_SERVER): имя файла и контрольную сумму, равную 0
        send_message(msgqid, (struct mymsg_buf*)&qbuf, TYPE_FOR_SERVER, argv[i], 0);
    }

    //ждем завершение всех процессов
    for(int i = 1; i < argc; i++)
    {
        waitpid(pid[i], 0, 0);
    }

    //принимаем из очереди сообщения с типом 2(TYPE_FOR_CLIENT)
    for(int i = 1; i < argc; i++)
    {
        read_message(msgqid, &qbuf, TYPE_FOR_CLIENT);
        if(qbuf.c_sum > 0)
            printf("Control sum for file %s = %ld\n", qbuf.filename, qbuf.c_sum);
    }

    //удаляем очередь
    if((rc = msgctl(msgqid, IPC_RMID, NULL)) < 0)
    {
        perror(strerror(errno));
        printf("msgctl (return queue) failed, rc=%d\n", rc);
        exit(4);
    }
    else
        printf("queue deleted\n");

    return 0;
}
