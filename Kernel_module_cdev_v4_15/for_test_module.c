#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define DEVICE "/dev/mydev1"
#define BUFF_SIZE 100

int main(int argc, char** argv)
{
    int fd;
    char ch, write_buf[BUFF_SIZE], read_buf[BUFF_SIZE];
    
    memset(write_buf,0,BUFF_SIZE-1);
    memset(read_buf,0,BUFF_SIZE-1);

    fd = open(DEVICE, O_RDWR);

    if (!fd)
        return -1;

    printf("type: r - for read, w - for write\n");
    scanf("%c", &ch);

    switch (ch)
    {
    case 'w':
        printf("enter data: ");
        scanf(" %[^\n]", write_buf);
        strcat(write_buf,"\n");
        write(fd, write_buf, sizeof(write_buf));
        break;
    case 'r':
        read(fd, read_buf, sizeof(read_buf));
        printf("mydev: %s\n", read_buf);
        break;
    }

    return 0;
}
