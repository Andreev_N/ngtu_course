#include <linux/module.h>
#include <linux/string.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>

#define MY_MAJOR  200
#define MY_MINOR  0
#define MY_DEV_COUNT 2  	//количество создавыемых устройст
#define MAX_STR_SIZE 100	//максимальная длина строки

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("A Simple Device Driver module ");

static int     my_open( struct inode *, struct file * );
static ssize_t my_read( struct file *,        char *, size_t, loff_t *);
static ssize_t my_write(struct file *, const  char *, size_t, loff_t *);
static int     my_close(struct inode *, struct file * );

struct file_operations my_fops =
{
    .read = my_read,
    .write = my_write,
    .open = my_open,
    .release = my_close,
    .owner = THIS_MODULE
};

/*символьное устройство*/
struct cdev my_cdev;

/*структура с данными для обмена с userspace*/
struct char_device
{
    char data[MAX_STR_SIZE];
} device;


int init_module(void)
{
    dev_t devno;
    unsigned int count = MY_DEV_COUNT;
    int err;
    /*конвертация MY_MAJOR, MY_MINOR в тип dev_t*/
    devno = MKDEV(MY_MAJOR, MY_MINOR);
    /*получение одного или нескольких номеров устройств*/
    register_chrdev_region(devno, count, "mydev");
    /*инициализация символьного устройства соответствующими операциями*/
    cdev_init(&my_cdev, &my_fops);
    my_cdev.owner = THIS_MODULE;
    /*передаем эту структуру в VFS с помощью вызова cdev_add()*/
    err = cdev_add(&my_cdev, devno, count);

    /*проверяем на наличие ошибок*/
    if (err < 0)
    {
        printk("Device Add Error\n");
        return -1;
    }
    /*вывод в журнал информации о добавленных устройствах*/
    printk("Hello World. This is my first char dev.\n");
    printk("'mknod /dev/mydev_0 c %d 0'.\n", MY_MAJOR);
    printk("'mknod /dev/mydev_1 c %d 1'.\n", MY_MAJOR);

    /*начальная инициализация строки*/
    strcpy(device.data,"Hello\n");

    return 0;
}


/*освобождение устройства*/
void cleanup_module(void)
{
    dev_t devno;
    printk("Goodbye\n");
    devno = MKDEV(MY_MAJOR, MY_MINOR);
    unregister_chrdev_region(devno, MY_DEV_COUNT);
    cdev_del(&my_cdev);
}


/*Файловая операция открытия*/
static int my_open(struct inode *inod, struct file *fil)
{
    int major;
    int minor;
    major = imajor(inod);
    minor = iminor(inod);
    printk("=== Some body is opening me at major %d  minor %d\n",major, minor);
    return 0;
}

/*Файловая операция чтения*/
static ssize_t my_read( struct file * file, char * buf,
                        size_t count, loff_t *ppos )
{
    int len = strlen( device.data );
    printk( KERN_INFO "=== read : %ld\n", count );
    if( count < len ) return -EINVAL;
    if( *ppos != 0 )
    {
        printk( KERN_INFO "=== read return : 0\n" );  // EOF
        return 0;
    }
    if( copy_to_user( buf, device.data, len ) ) return -EINVAL;
    *ppos = len;
    printk( KERN_INFO "=== read return : %d\n", len );
    return len;
}

/*Файловая операция записи*/
static ssize_t my_write(struct file *filp, const char *buff, size_t len, loff_t *off)
{
    short count;
    memset(device.data, 0, MAX_STR_SIZE-1);

    /*копирование строки из userspace*/
    count = copy_from_user( device.data, buff, len );

    printk("=== Msg from userspace: %s", device.data);

    return len;
}


/*Файловая операция закрытия*/
static int my_close(struct inode *inod, struct file *fil)
{
    int minor;
    minor = MINOR(fil->f_inode->i_rdev);
    printk("=== Some body is closing me at minor %d\n",minor);
    return 0;
}
