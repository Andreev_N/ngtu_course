#include <stdlib.h> 
#include <string.h> 
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/socket.h>
#include <unistd.h>     
#include <arpa/inet.h> 
#include <pthread.h>
#include <signal.h>

#include "amessage.pb-c.h"

#define SLEEP_TIME 10       //интервал отправки оповещений 
#define MAXPENDING 5        //длина очереди для listen
#define MSGPERM 0600        // права доступа к очереди
#define MSGKEY 32769        //ключ для очереди
#define MSGTYPE 1           //тип для очереди
#define MAX_QUEUE_SIZE 10   //максимальный размер очереди
#define MAX_BUF_SIZE 1024   //максимальный размер принятого сообщения

#define TCP_PORT_RECV 4444 //порт для приема сообщений
#define TCP_PORT_SEND 4445 //порт для отправки сообщений

#define CLIENT1_PORT_MIN 6000 //диапазон портов клиентов 1го типа
#define CLIENT1_PORT_MAX 6010 

#define CLIENT2_PORT_MIN 7000 //диапазон портов клиентов 2го типа
#define CLIENT2_PORT_MAX 7010 

/*структура данных для потоков*/
struct DATA{
	int port;
	int msgqid;
};
typedef struct DATA Data;

/*структура UDP оповещений*/
struct udp_alarm
{
    int type_client;
    int serv_port;
};

/*структура сообщений клиентов*/
struct protocol
{
    int time;
    int len;
    char *text;
};

/*структура сообщения в очереди*/
struct mymsg_buf
{
    long mtype;
    uint8_t *unserial_msg[MAX_BUF_SIZE];
};

/*завершение программы с текстом ошибки*/
void dieWithError(char* errorMessage);

/*приём сообщения (TCP) и помещения его в очередь */
void recv_msg(int clntSocket, int msgqid);

/*получение сообщения из очереди и его отправка(TCP)  */
void send_msg(int clntSocket, int msgqid);

/*потоковая функция TCP для приема сообщений клиентов 1го типа*/
void* tcp_recv(void* arg);

/*потоковая функция TCP для отправки сообщений клиентам 2го типа*/
void* tcp_send(void* arg);

/*потоковая функция UDP для отправки оповещений клиентам 1го типа*/
void* udp_first_clients(void* arg);

/*потоковая функция UDP для отправки оповещений клиентам 2го типа*/
void* udp_second_clients(void* arg);

/*создание потоков*/
void create_threads(pthread_t *tcp_first,pthread_t *tcp_second,pthread_t *udp_first,pthread_t *udp_second, Data *thread_data);

/*присоединение все потоков (после нажатися ctrl+c)*/
void join_all_threads(pthread_t tcp_first,pthread_t tcp_second,pthread_t udp_first,pthread_t udp_second);
