#!/bin/bash
#запуск Сервера, двух клиентов 1го типа, одного клиента 2го типа(при условии что make выполнен)
if [[ -e ./Server/server && -e ./Client1/client1 && -e ./Client2/client2 ]]
then
xterm -geometry 105x25+10+10 -e ./Server/server &
xterm -geometry 105x25+1000+10 -e ./Client1/client1 127.0.0.1 6000 &
xterm -geometry 105x25+1000+1000 -e ./Client1/client1 127.0.0.1 6001 &
xterm -geometry 105x25+10+1000 -e ./Client2/client2 127.0.0.1 7000
else 
echo "Сначала выполните make сервера и 2х клиентов"
fi
