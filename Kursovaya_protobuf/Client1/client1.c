#include <arpa/inet.h> 
#include <stdio.h>      
#include <stdlib.h>   
#include <string.h>     
#include <sys/socket.h> 
#include <unistd.h>    
#include <time.h>

#include "header.h"

void DieWithError(char* errorMessage)
{
    perror(errorMessage);
    exit(1);
}

void Serialize_and_send(struct protocol my_protocol, int sock)
{
    AMessage msg = AMESSAGE__INIT; // AMessage
    void *buf;                     // буфер для серилизованных данных
    unsigned len;                  // длина серилизованных данных
    int result;                    // для проверки результатов
    
    /*заполнение полей AMessage*/
    msg.time=my_protocol.time;
    msg.len=my_protocol.len;
    msg.text=my_protocol.text;
    
    /*вычисление длины AMessage*/
    len = amessage__get_packed_size(&msg);
    
    buf = malloc(len);
    /*серилизация*/
    amessage__pack(&msg,buf);
    
    printf("Send %d serialized bytes\n",len); 
    printf("Send message: time:[%d]; length:[%d]; random string: [%s]\n",my_protocol.time,my_protocol.len,my_protocol.text);
    printf("------------------------\n");
    /*отправка сообщения серверу*/
    result = send(sock, buf, len, 0);
    if(result < 0)
        DieWithError("send() failed");

    free(buf);
}

/*генерация рандомной строки указанной длины*/
char* random_string(int len)
{
    const char alph[] = "0123456789"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "abcdefghijklmnopqrstuvwxyz";

    char* buf = (char*)calloc(len+1,sizeof(char));
    int i;

    for(i = 0; i < len; i++)
        buf[i] = alph[rand() % (sizeof(alph) - 1)];

    return buf;
}

/*отправка сообщения серверу*/
void Send_msg_to_server(char* servIP, int ServPort)
{
    int sock;
    struct sockaddr_in ServAddr;    //структура адреса сервера
    unsigned int StringLen;         //длина строки
    struct protocol proto;          //структура сообщений клиентов
    
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        DieWithError("socket() failed");

    memset(&ServAddr, 0, sizeof(ServAddr));
    ServAddr.sin_family = AF_INET;
    ServAddr.sin_addr.s_addr = inet_addr(servIP);
    ServAddr.sin_port = htons(ServPort);

    if(connect(sock, (struct sockaddr*)&ServAddr, sizeof(ServAddr)) < 0)
        DieWithError("connect() failed");

    /*случайный выбор длины строки*/
    StringLen = rand() % MAX_STRING_LEN + 1;

    /*формирование сообщения*/
    proto.time = rand() % MAX_SLEEP_TIME;
    proto.len = StringLen;
    proto.text=random_string(StringLen);
    
    Serialize_and_send(proto,sock);   

    close(sock);
    free(proto.text);
}

/*создание и биндинг сокета*/
int create_bind_socket(int port)
{
    int mysocket;
    struct sockaddr_in local_addr;

    /*создание сокета*/
    if((mysocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        DieWithError("socket() failed");
    }
    /*связывание сокета с локальным адресом*/
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(port);
    local_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    /*биндинг*/
    if(bind(mysocket, (struct sockaddr*)&local_addr, sizeof(local_addr)))
    {
        DieWithError("connect() failed");
    }
    return mysocket;
}

void wait_upd(int mysocket, char* servIP)
{
    int bytes_recv; //количество принятых байт
    struct udp_alarm recv_msg; //структура оповещений UDP
    struct sockaddr_in serv_addr; // структура адреса udp сервера
    int serv_addr_size = sizeof(struct sockaddr_in);

    while(1)
    {
        // прием датаграммы
        bytes_recv = recvfrom(mysocket, &recv_msg, sizeof(struct udp_alarm), 0, (struct sockaddr*)&serv_addr,
                              (socklen_t*)&serv_addr_size);
        if(bytes_recv < 0)
        {
            DieWithError("recvfrom() failed");
        }
        else
        {
            printf("------------------------\n");
            printf("Get upd request string; TCP port:[%d]\n",recv_msg.serv_port);
        }

        /*отправка сообщения серверу на указанный порт (TCP)*/
        Send_msg_to_server(servIP, recv_msg.serv_port);
        /*засыпание на случайное время*/
        sleep(1+rand()%TIME_FOR_SLEEP);
    }
}

int main(int argc, char* argv[])
{
    unsigned short my_port; //порт клиента
    char* ServIP;           //IP адрес сервера
    int mysocket;           //сокет

    srand(getpid());      //для генерации различных строк

    if((argc < 2) || (argc > 3))
    {
        fprintf(stderr, "Usage: %s <Server IP> <Echo Port (6000-6010)>\n", argv[0]);
        exit(1);
    }

    ServIP = argv[1];
    my_port = atoi(argv[2]);

    /*создание и биндинг сокета к адресу*/
    mysocket = create_bind_socket(my_port);

    /*ожидание UDP оповещений*/
    wait_upd(mysocket, ServIP);

    exit(0);
}
