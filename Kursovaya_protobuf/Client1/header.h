#include "amessage.pb-c.h"

#define TIME_FOR_SLEEP 8    //время для засыпания
#define MAX_BUF_SIZE 1024   //максимальный размер сообщения

#define MAX_STRING_LEN 15   //максимальная длина генерируемоц строки
#define MAX_SLEEP_TIME 1000 //максимальное время поля time
#define MAX_BUF_SIZE 1024   //максимальный размер сообщения

struct protocol
{
    int time;
    int len;
    char* text;
};

struct udp_alarm
{
    int type_client;
    int serv_port;
};