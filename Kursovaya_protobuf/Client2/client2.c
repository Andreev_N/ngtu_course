#include <arpa/inet.h> 
#include <stdio.h>   
#include <stdlib.h>   
#include <string.h>     
#include <sys/socket.h> 
#include <unistd.h>    
#include <time.h>

#include "header.h"

void DieWithError(char* errorMessage)
{
    perror(errorMessage);
    exit(1);
}

int create_bind_socket(int port)
{
    int mysocket;
    struct sockaddr_in local_addr;

    /*создание сокета*/
    if((mysocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        DieWithError("socket() failed");
    }
    /*связывание сокета с локальным адресом*/
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(port);
    local_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    /*биндинг*/
    if(bind(mysocket, (struct sockaddr*)&local_addr, sizeof(local_addr)))
    {
        DieWithError("connect() failed");
    }
    return mysocket;
}

void recv_msg_from_server(char* servIP, int ServPort)
{
    int sock;
    struct sockaddr_in ServAddr;
    uint8_t buf[MAX_BUF_SIZE];
    AMessage *msg;
    int recvMsgSize;

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        DieWithError("socket() failed");

    memset(&ServAddr, 0, sizeof(ServAddr));
    ServAddr.sin_family = AF_INET;
    ServAddr.sin_addr.s_addr = inet_addr(servIP);
    ServAddr.sin_port = htons(ServPort);

    if(connect(sock, (struct sockaddr*)&ServAddr, sizeof(ServAddr)) < 0)
        DieWithError("connect() failed");   
           
    recvMsgSize = recv(sock, &buf, MAX_BUF_SIZE, 0);
    if(recvMsgSize < 0)
        DieWithError("recv() failed");
    
    /*Десерилизация принятого сообщения*/
    msg = amessage__unpack(NULL, recvMsgSize, buf);
    
    if (msg == NULL)
    {
        printf("error unpacking incoming message\n");
        exit(1);
    }
    
    printf("Get message: time:[%d]; lenght:[%d]; text:[%s]\n", msg->time, msg->len,msg->text);
    printf("------------------------\n");
    amessage__free_unpacked(msg, NULL);
    
    close(sock);
    
}

void wait_upd(int mysocket, char* servIP)
{
    int bytes_recv;
    struct udp_alarm recv_msg;
    struct sockaddr_in serv_addr; // структура адреса udp сервера
    int serv_addr_size = sizeof(struct sockaddr_in);

    while(1)
    {
        // прием датаграммы
        bytes_recv = recvfrom(mysocket, &recv_msg, sizeof(struct udp_alarm), 0, (struct sockaddr*)&serv_addr,
            (socklen_t*)&serv_addr_size);
        if(bytes_recv < 0)
        {
            DieWithError("recvfrom() failed");
        }
        else
        {
            printf("------------------------\n");
            printf("Get upd request; TCP port:[%d]\n", recv_msg.serv_port);
        }
        /*прием соощения от сервера*/
        recv_msg_from_server(servIP, recv_msg.serv_port);
        /*засыпание на случайное время*/
        sleep(1 + rand() % TIME_FOR_SLEEP);
    }
}

int main(int argc, char* argv[])
{
    unsigned short my_port; 
    char* servIP; 
    int mysocket;
    
    srand(time(NULL));
    
    if((argc < 2) || (argc > 3))
    {
        fprintf(stderr, "Usage: %s <Server IP> <Port (7000-7010)>\n", argv[0]);
        exit(1);
    }

    servIP = argv[1];
    my_port = atoi(argv[2]);
    
    /*создание и биндинг сокета к адресу*/
    mysocket = create_bind_socket(my_port);
    /*ожидание UDP оповещений*/
    wait_upd(mysocket, servIP);

    exit(0);
}