#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>

#include "header.h"

int main(int argc, char* argv[])
{
    int shmid;// id разделяемой памяти
    int* shm_pid;//начало разделяемой памяти
    int size; //размер разделяемой памяти
    int random_number;//номер случайного процесса    
    int num = 0; //число живых процессов
    int my_pid = getpid(); //пид процесса
    int semid; //id семафора
    struct sembuf lock_res = { 0, -1, 0 }; //блокировка ресурса
    struct sembuf rel_res = { 0, 1, 0 };   //освобождение ресурса

    if(argc < 2)
    {
        printf("Usage: %s number of processes\n", argv[0]);
        exit(0);
    }
    
    /* Подключаем созданный семафор*/
    semid = semget(KEY, 0, PERMITONS);

    size = sizeof(int) * (atoi(argv[1]) + 1);

    /* Подключаем область разделяемой памяти */
    if((shmid = shmget(KEY, size, PERMITONS)) < 0)
    {
        perror("shmget");
        exit(1);
    }
    /* Получаем доступ к разделяемой памяти */
    if((shm_pid = shmat(shmid, NULL, 0)) == (int*)-1)
    {
        perror("shmat");
        exit(1);
    }

    srand(getpid());

    while(num != 1) //пока не останется один процесс в живых
    {
        num = 0;
        sleep(1 + rand() % MAX_RNG_TIME); //спим случайное время

        /* Заблокируем разделяемую память */
        if((semop(semid, &lock_res, 1)) == -1)
        {
            fprintf(stderr, "Lock failed\n");
            exit(1);
        }      

        do //выбираем рандомный PID, пока не выбрали ЧУЖОЙ PID и не нулевой PID
        {
            random_number = 1 + rand() % (atoi(argv[1]));
        } while(shm_pid[random_number] == my_pid || shm_pid[random_number] == 0);

        kill(shm_pid[random_number], SIGINT); //отправка сигнала на завершение выбранного процесса
        printf("Process [%d] choose PID %d for kill\n", my_pid, shm_pid[random_number]);
        shm_pid[random_number] = 0;
        
        /*Подсчет ненулевых PIDов*/
        for(int i = 1; i <= atoi(argv[1]); i++)
        {
            if(shm_pid[i] != 0)
                num++;
        }
        /*если PID остался один в массиве, то он и есть победитель*/
        if(num == 1)
        {
            if(shmdt(shm_pid) < 0) //выгружаем разделяемую память
            {
                printf("Ошибка отключения\n");
                exit(1);
            }
            printf("Im [%d] the last one.\n", my_pid);
            exit(1);
        }
        
        /* Освободим разделяемую память */
        if((semop(semid, &rel_res, 1)) == -1)
        {
            fprintf(stderr, "Unlock failed\n");
            exit(1);
        }
    }

    return 0;
}
