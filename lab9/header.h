#ifndef __HEADER_H__
#define __HEADER_H__

#define PERMITONS 0666
#define KEY 32769
#define MAX_RNG_TIME 10
#define SIZE_BUF 10

union semun 
{
    int val;               /* значение для SETVAL */
    struct semid_ds* buf;  /*буферы для IPC_STAT, IPC_SET */
    unsigned short* array; /* массивы для GETALL, SETALL */
    struct seminfo* __buf; /* буфер для IPC_INFO */
};

#endif
