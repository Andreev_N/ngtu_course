#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

#include "header.h"

int main(int argc, char* argv[])
{
    int count_process;  //число процессов
    int* pid;           //для массива PIDов
    int* shm_pids;      //начало разделяемой памяти
    int size;           //размер разделяемой памяти
    int shmid;          // id разделяемой памяти
    char buf[SIZE_BUF]; //для конвертации из чисел в строку через sprintf
    int semid;          //семафор
    union semun arg;    //структура семафора

    printf("Enter number of process\n");
    scanf("%d", &count_process);
    printf("The hunger games is begin! \n");

    /*Создание семафора*/
    semid = semget(KEY, 1, PERMITONS | IPC_CREAT);
    /*Установка в семафоре единицы */
    arg.val = 1;
    semctl(semid, 0, SETVAL, arg);

    size = sizeof(int) * (count_process + 1);
    pid = (int*)malloc(size);

    /* создаём участок разделяемой памяти */
    if((shmid = shmget(KEY, size, PERMITONS | IPC_CREAT)) < 0)
    {
        perror("shmget");
        exit(1);
    }
    
    sprintf(buf, "%d", count_process);

    for(int i = 1; i <= count_process; i++)
    {
        pid[i] = fork();
        if(pid[i] == 0)
        { /*Ребенок*/
            if(execl("./server", "server", buf, NULL) < 0)
            {
                printf("ERROR while start processing file %d\n", i);
                exit(2);
            }
        }
    }
    
    /* Получим доступ к разделяемой памяти*/
    if((shm_pids = shmat(shmid, NULL, 0)) == (int*)-1)
    {
        perror("shmat");
        exit(1);
    }
    printf("Pretenders:\n");
    
    /*Пишем номера процессов в разделяемую память*/
    for(int i = 1; i <= count_process; i++)
    {
        shm_pids[i] = pid[i];
        printf("Pid #%d - [%d]\n", i, pid[i]);
    }

    /*Ждем завершение всех процессов*/
    for(int i = 1; i <= count_process; i++)
    {
        waitpid(pid[i], 0, 0);
    }
    
    /*Вывод победителя*/
    for(int i = 1; i <= count_process; i++)
    {
        if(shm_pids[i] != 0)
        {
            printf("-----------------------------\n");
            printf("Winner is PID number - [%d]\n", shm_pids[i]);
            printf("-----------------------------\n");
        }
    }
    /*выгружаем разделяемую память*/
    if(shmdt(shm_pids) < 0)
    {
        printf("Ошибка отключения\n");
        exit(1);
    }

    /* Удаляем созданные объекты IPC */
    if(shmctl(shmid, IPC_RMID, 0) < 0)
    {
        printf("Невозможно удалить область\n");
        exit(1);
    }

    if(semctl(semid, 0, IPC_RMID) < 0)
    {
        printf("Невозможно удалить семафор\n");
        exit(1);
    }
    
    free(pid);

    return 0;
}
