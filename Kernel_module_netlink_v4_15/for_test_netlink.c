#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <linux/netlink.h>

#define NETLINK_USER 31

#define MAX_PAYLOAD 1024 /* максимальный размер поля данных*/

struct sockaddr_nl src_addr, dest_addr;
int sock_fd;

/*объявление структур для netlink пакета*/
struct nlmsghdr *nlh = NULL;
struct iovec iov;
struct msghdr msg;


int main(int argc, char** argv)
{
    /*создаем сырой сокет для связи с kernel через netlink с netlink_family=NETLINK_USER*/
    sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
    if (sock_fd < 0)
        return -1;

    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = AF_NETLINK;
    src_addr.nl_pid = getpid(); /* собственный pid (чтобы kernel знал какому процессу отправлять ответ)*/

    bind(sock_fd, (struct sockaddr *)&src_addr, sizeof(src_addr));

    memset(&dest_addr, 0, sizeof(dest_addr));
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;
    dest_addr.nl_pid = 0; /* Для Kernel поле pid должно быть равно нулю */
    dest_addr.nl_groups = 0; /* unicast */

    /*выделяем память для формируемого пакета*/
    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
    /*формирование пакета данных*/
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
    nlh->nlmsg_pid = getpid();
    nlh->nlmsg_flags = 0;
    /*формирование поле данных*/
    strcpy(NLMSG_DATA(nlh), "Hello");

    /*заполнение и связывание полей структур iovec и msghdr с nlmsghdr*/
    iov.iov_base = (void *)nlh;
    iov.iov_len = nlh->nlmsg_len;
    msg.msg_name = (void *)&dest_addr;
    msg.msg_namelen = sizeof(dest_addr);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    printf("Sending message to kernel\n");
    sendmsg(sock_fd, &msg, 0);
    printf("Waiting for message from kernel\n");

    /*чтение сообщения из ядра*/
    recvmsg(sock_fd, &msg, 0);
    printf("Received message payload: %s\n", (char *)NLMSG_DATA(nlh));
    close(sock_fd);
    free(nlh);

    return 0;
}
