#define _GNU_SOURCE
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define MAX_NAME_SIZE 50
#define BUF_SIZE 1024

// Функция обработки ошибок
void error(const char* msg)
{
    perror(msg);
    exit(0);
}

int create_binding_socket(struct sockaddr_in* server, char* host, char* port)
{
    int sock;
    struct hostent* hp; // структура хоста
    /*создание сокета*/
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock < 0)
        error("socket");
    /*связывание*/
    server->sin_family = AF_INET; // указание адресации
    hp = gethostbyname(host);     // извлечение хоста
    if(hp == 0)
        error("Unknown host");
    bcopy((char*)hp->h_addr, (char*)&server->sin_addr, hp->h_length);
    server->sin_port = htons(atoi(port)); // извлечение порта
    return sock;
}

int main(int argc, char* argv[])
{
    int sock, n;         // дескрипторы
    unsigned int length; // размер структуры адреса
    struct sockaddr_in server; // структуры адреса сервера 
    char filename[MAX_NAME_SIZE]; // буфер для имени файлп
    char buf[BUF_SIZE];           //буфер
    int file, numRead;            //дескриптор файла и количество считанных байт
    int i;                        //счетчик
    long int sum = 0;             //контрольная сумма
    struct sockaddr_in server_addr;  
    int server_addr_size = sizeof(server_addr);

    length = sizeof(struct sockaddr_in); //размер адреса

    if(argc != 3)
    {
        printf("Usage: host server_port\n");
        exit(1);
    }

    /*создание сокета и инициализация адреса struct sockaddr_in server*/
    sock = create_binding_socket(&server, argv[1], argv[2]);

    //Прием и отправка датаграмм
    while(1)
    {
        printf("----------------------------------\n");
        printf("Enter <path to file> or quit\nS<=C:");

        bzero(buf, BUF_SIZE);
        bzero(filename, MAX_NAME_SIZE);

        file = -1;
        /*Повтор ввода имени(пути) файла, пока не будет введн существующий*/
        while(file == -1)
        {

            fgets(&filename[0], sizeof(filename) - 1, stdin);
            filename[strlen(filename) - 1] = '\0'; //убирает \n
            // проверка на quit
            if(!strcmp(&filename[0], "quit"))
            {
                close(sock);
                exit(1);
            }
            file = open(filename, O_RDONLY);
            if(file == -1)
            {
                printf("File not found !\nS<=C:");
            }
        }

        numRead = 0;
        i = 0;
        do
        {
            // чтение одного символа из файла
            numRead = read(file, &buf[i++], 1);

        } while(numRead > 0 && i < BUF_SIZE);

        close(file);

        /*отправка буфера серверу*/
        n = sendto(sock, &buf[0], i, 0, (const struct sockaddr*)&server, length);
        if(n < 0)
            error("Sendto");
        printf("send %d bytes\n", n);

        // прием датаграммы
        n = recvfrom(sock, &sum, sizeof(long int), 0, (struct sockaddr*)&server_addr, (socklen_t*)&server_addr_size);
        if(n < 0)
            error("recvfrom");
        printf("S=>C:Control sum = [%ld]\n", sum);
    }
    /*закрытие сокета*/
    close(sock);
    return 0;
}
