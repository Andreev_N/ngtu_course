#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define BUF_SIZE 1024

long int ctrl_sum(char* buffer, int size)
{
    long int sum = 0;

    for(int i = 0; i <= size; i++)
    {
        sum += (unsigned char)buffer[i];
    }
    return sum;
}

// Функция обработки ошибок
void error(const char* msg)
{
    perror(msg);
    exit(0);
}

/*функция ответа на запросы*/
void answer_to_request(int socket, struct sockaddr* client_addr, char* buf)
{
    int n;
    long int sum = 0;
    sum = ctrl_sum(buf, strlen(buf));
    printf("S=>C:Control sum = [%ld]\n", sum);
    // посылка датаграммы клиенту
    n = sendto(socket, &sum, sizeof(long int), 0, client_addr, sizeof(*client_addr));
    if(n < 0)
        error("sendto");
}

int create_bind_socket(int port)
{
    int mysocket;
    struct sockaddr_in local_addr;

    /*создание сокета*/
    if((mysocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        error("Opening socket");
    }
    /*связывание сокета с локальным адресом*/
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(port);
    local_addr.sin_addr.s_addr = INADDR_ANY;

    /*binding*/
    if(bind(mysocket, (struct sockaddr*)&local_addr, sizeof(local_addr)))
    {
        error("binding");
    }
    return mysocket;
}
/*Определяем IP-адрес клиента и прочие атрибуты и выводим на экран*/
void show_info(struct sockaddr_in client_addr)
{
    struct hostent* hst;
    hst = gethostbyaddr((char*)&client_addr.sin_addr, 4, AF_INET);
    printf("+%s [%s:%d] new DATAGRAM!\n", (hst) ? hst->h_name : "Unknown host", (char*)inet_ntoa(client_addr.sin_addr),
        ntohs(client_addr.sin_port));
}

void server(int port)
{
    struct sockaddr_in client_addr; // структура адреса клиента
    char buf[BUF_SIZE];
    int pid;
    int mysocket;
    int bytes_recv;
    int client_addr_size = sizeof(struct sockaddr_in);

    /*создание сокета*/
    mysocket = create_bind_socket(port);

    /*прием и передача датаграмм*/
    while(1)
    {
        bzero(buf, BUF_SIZE);
        // прием датаграммы
        bytes_recv = recvfrom(
            mysocket, &buf[0], sizeof(buf) - 1, 0, (struct sockaddr*)&client_addr, (socklen_t*)&client_addr_size);
        if(bytes_recv < 0)
        {
            error("recvfrom");
        }
        
        /*Определяем IP-адрес клиента и прочие атрибуты и выводим на экран*/
        show_info(client_addr);
        
        /*Выводим количество принятых байт*/
        printf("Get %d bytes\n", bytes_recv);

        /*обработка запроса в отдельном процессе*/
        pid = fork();
        if(pid < 0)
            error("ERROR on fork");
        if(pid == 0)
        {
            answer_to_request(mysocket, (struct sockaddr*)&client_addr, &buf[0]);
            close(mysocket);
            exit(0);
        }
        /*не ждем завершения дочернего процесса*/
        waitpid(pid, 0, WUNTRACED);
    }
    close(mysocket);
}

int main(int argc, char* argv[])
{
    printf("Server UDP for ctrl_sum\n");

    if(argc < 2)
    {
        fprintf(stderr, "ERROR, no port provided\n");
        exit(0);
    }

    server(atoi(argv[1]));

    return 0;
}
