#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/kernel.h>

MODULE_LICENSE( "GPL" );

#define MAX_MSG_SIZE 100
static char buf_msg[ MAX_MSG_SIZE + 1 ] = "Hello from module!\n";


/* Метод show() из sysfs API */
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,32)
static ssize_t my_show( struct class *class, struct class_attribute *attr, char *buf )
{
#else
static ssize_t my_show( struct class *class, char *buf )
{
#endif
    strcpy( buf, buf_msg );
    printk(KERN_INFO "read %ld\n", strlen( buf ) );
    return strlen( buf );
}

/* Метод store() из sysfs API */
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,32)
static ssize_t my_store( struct class *class, struct class_attribute *attr, const char *buf, size_t count )
{
#else
static ssize_t my_store( struct class *class, const char *buf, size_t count )
{
#endif
    printk(KERN_INFO "write %ld\n", count );
    strncpy( buf_msg, buf, count );
    buf_msg[ count ] = '\0';
    return count;
}

/*создание атрибутной записи типа struct class_attribute*/
CLASS_ATTR( my_sys, 0666, &my_show, &my_store);

static struct class *my_class;

int __init my_init(void)
{
    int res;
    my_class = class_create( THIS_MODULE, "my_class" );
    if( IS_ERR( my_class ) ) printk(KERN_INFO "bad class create\n" );
    res = class_create_file( my_class, &class_attr_my_sys );

    printk(KERN_INFO "'xxx' module initialized\n");
    return 0;
}

void my_cleanup(void)
{
    class_remove_file( my_class, &class_attr_my_sys );
    class_destroy( my_class );
    return;
}

module_init( my_init );
module_exit( my_cleanup );
