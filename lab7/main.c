#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <locale.h>
#include <wchar.h> //для считывания русских символов через fgetwc

#define SIZE_BUF 10

void server(int, int);

int main(int argc, char* argv[])
{    
    int pid[argc];      //для массива PIDов
    int status, stat;   //статус и возвращаемое значение для waitpid
    long int sum=0;     //комнтрольная сумма
    int fd1[argc][2];   //канал 1
    int fd2[argc][2];   //канал 2
    setlocale(LC_ALL,""); //для русских символов

    if(argc < 2)
    {
        printf("Usage: ./main file file ...\n");
        exit(-1);
    }
    
    for(int i = 1; i < argc; i++)
    {
        pipe(fd1[i]);
        pipe(fd2[i]);

        pid[i] = fork();
        if(pid[i] == 0)
        {   /*Ребенок (сервер)*/
            close(fd1[i][1]);
            close(fd2[i][0]);
            server(fd1[i][0], fd2[i][1]);
            exit(0);
        }
    }
    
    /*Родитель (клиент)*/ 
    //передача имени файла по каналу fd1
    for(int i = 1; i < argc; i++)
    {
        close(fd1[i][0]);
        close(fd2[i][1]);
        write(fd1[i][1], argv[i], strlen(argv[i]));
        close(fd1[i][1]); //для EOF
    }
    
    //прием контрольной суммы через канал fd2
    for(int i = 1; i < argc; i++)
    {
        read(fd2[i][0], &sum, sizeof(long int));
        if (sum > 0)
        printf("Control sum for file %s = %ld\n",argv[i], sum);
    }
    
    //ждем завершение всех процессов
    for(int i = 1; i < argc; i++)
    {
        status = waitpid(pid[i], &stat, 0);
        if(pid[i] == status)
        {
            printf("Childe process #%d done,  result=%d (0-success;1-no such file)\n", i, WEXITSTATUS(stat));
        }
    }

    return 0;
}
