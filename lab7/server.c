#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <locale.h>
#include <wchar.h>

#define MAX_SIZE_NAME 30

void server(int readfd, int writefd)
{
    
    FILE *file;
    char file_name[MAX_SIZE_NAME];
    wchar_t ch; 
    long int sum=0;

    read(readfd, file_name, MAX_SIZE_NAME);

    if((file = fopen(file_name, "r")) == NULL)
    {
        write(writefd, &sum, sizeof(long int)); //отправляем 0
        exit(1);
    }

    while((ch = fgetwc(file)) != EOF)
    { 
        sum+=ch;
    }    
    
    fclose(file);

    write(writefd, &sum, sizeof(long int)); //записываем найденную сумму
}
