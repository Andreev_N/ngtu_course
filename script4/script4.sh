#!/bin/bash

#полный путь к скрипту
script_path=/home/nikolay/Desktop/linux_labs/script4.sh

function create_task {
#ввод параметров периодичности и имени процесса(вынесен в отдельную функцию)
enter_param
#заносим cronetab в cron.txt
crontab -l > cron.txt
#добавление задания (запуск скрипта с параметром имени процесса)
echo "*/$period * * * * $script_path $name" >> cron.txt
#обновление crontab
crontab cron.txt   
echo -e "\n*/$period * * * * $script_path $name task add"
}

function delete_task {
#заносим в файл cron.txt только задания нашего скрипта (поиск по setsid)
crontab -l | grep 'script4.sh' > cron.txt
#вывод текущих заданий с нумерацией
cat -n cron.txt
echo "-----^-------------------------------"
echo "Enter Number of delete task (1column):"
read col
#введеное число должен быть больше числа строк cron.txt и больше нуля
while [[ $col -gt $(cat cron.txt | wc -l)  || $col -le 0 ]]
do
    echo "Wrong number!try again"
    read col
done
#запоминаем строку по её номеру 
str_for_del=$(sed -n ${col}p cron.txt)
#заносим в файл cron.txt все задания (не только нашего скрипта)
crontab -l > cron.txt
#экранирование слэшей и звездочек
str=$(sed 's/[\*\/]/\\&/g' <<<"$str_for_del")
#удаляем найденную строку
sed -i "/${str}/d" cron.txt
#обновляем crontab
crontab cron.txt
echo -e "task deleted"   
}

function edit_task {
#заносим в файл cron.txt только задания для контроля (поиск по setsid)
crontab -l | grep 'script4.sh' > cron.txt
#вывод текущих заданий с нумерацией
cat -n cron.txt
echo "-----^-------------------------------"
echo "Enter Number of edit task (1column):"
read col
#введеное число должен быть больше числа строк cron.txt и больше нуля
while [[ $col -gt $(cat cron.txt | wc -l)  || $col -le 0 ]]
do
    echo "Wrong number!try again"
    read col
done
#запоминаем строку по её номеру
str_for_edit=$(sed -n ${col}p cron.txt)
#заносим в файл cron.txt все задания (не только нашего скрипта)
crontab -l > cron.txt
#экранирование слэшей и звездочек
str=$(sed 's/[\*\/]/\\&/g' <<<"$str_for_edit")
#ввод новых параметров
echo "Enter new parametrs"
enter_param

#замена строки
sed -i "s~$str~*/$period * * * * $script_path $name~" cron.txt

#обновляем crontab
crontab cron.txt
echo -e "task edited"   
}


function enter_param {
#ввод параметров
echo "Enter process name"
read name
echo "Choose period in minute"
read period 
}


#Если скрипту передали параметр имени(в crontab добавляются имя этого скрипта и имя процесса 1м параметром), то выполняем его запуск(если он был прерван), или ничего не делаем(если он работает)
#Если параметров нет - то сразу вызывается меню действий
if [ $# -gt 0 ]
then
#смотрим запущен ли процесс(pidof покажет PIDы) и записываем результат(0 или 1) в result
pidof $1>>/dev/null
result="$?"
#0-запущен 1-не запущен
if [ $result -ne 0 ]
then 
$1
fi
#завершаем скрипт
exit 1
fi

#бесконечный цикл, пока не введено действие выхода
while true
do
echo "Enter action"
echo "1-create task; 2-edit task; 3-delete task; 4-exit"
read action
case $action in
     1)
          create_task
          ;;
     2)
          edit_task
          ;;
     3)
          delete_task
          ;; 
     4)
          echo "Exit..."
	  break
          ;;
     *)
          echo "Wrong action! Try again"
          ;;
esac
done

