#include <stdio.h>

#include "my_show_packet.h"

/*Вывод заголовка ethernet кадра*/
void ethernet_header(unsigned char* buffer,int buflen)
{
    struct ethhdr *eth = (struct ethhdr *)(buffer);
    printf("\nEthernet Header\n");
    printf("\t|-Source Address	: %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n",eth->h_source[0],eth->h_source[1],eth->h_source[2],eth->h_source[3],eth->h_source[4],eth->h_source[5]);
    printf("\t|-Destination Address	: %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n",eth->h_dest[0],eth->h_dest[1],eth->h_dest[2],eth->h_dest[3],eth->h_dest[4],eth->h_dest[5]);
    printf("\t|-Protocol		: %d\n",eth->h_proto);

}

/*Вывод ip заголовка*/
void ip_header(unsigned char* buffer,int buflen,int* iphdrlen)
{
    struct iphdr *ip = (struct iphdr*)(buffer + sizeof(struct ethhdr));
    struct sockaddr_in source,dest;

    *iphdrlen =ip->ihl*4;

    memset(&source, 0, sizeof(source));
    source.sin_addr.s_addr = ip->saddr;
    memset(&dest, 0, sizeof(dest));
    dest.sin_addr.s_addr = ip->daddr;

    printf("\nIP Header\n");
    printf("\t|-Version           : %d\n",(unsigned int)ip->version);
    printf("\t|-Internet Header Length  : %d DWORDS or %d Bytes\n",(unsigned int)ip->ihl,((unsigned int)(ip->ihl))*4);
    printf("\t|-Type Of Service   : %d\n",(unsigned int)ip->tos);
    printf("\t|-Total Length      : %d  Bytes\n",ntohs(ip->tot_len));
    printf("\t|-Identification    : %d\n",ntohs(ip->id));
    printf("\t|-Time To Live	  : %d\n",(unsigned int)ip->ttl);
    printf("\t|-Protocol 	      : %d\n",(unsigned int)ip->protocol);
    printf("\t|-Header Checksum   : %d\n",ntohs(ip->check));
    printf("\t|-Source IP         : %s\n", inet_ntoa(source.sin_addr));
    printf("\t|-Destination IP    : %s\n",inet_ntoa(dest.sin_addr));
}

/*Вывод полезной нагрузки*/
void payload(unsigned char* buffer,int buflen,int* iphdrlen)
{
    int i=0;
    unsigned char * data = (buffer + (*iphdrlen)  + sizeof(struct ethhdr) + sizeof(struct udphdr));
    /*Вычисляем размер нагрузки*/
    int remaining_data = buflen - ((*iphdrlen)  + sizeof(struct ethhdr) + sizeof(struct udphdr));

    printf("\nData\n");
    for(i=0; i<remaining_data; i++)
    {
        if(i!=0 && i%16==0)
            printf("\n");
        printf(" %.2X ",data[i]);
    }
    printf("\n");
}

/*Вывод tcp заголовка*/
void tcp_header(unsigned char* buffer,int buflen)
{
    int iphdrlen;
    struct tcphdr *tcp;
    printf("\n*************************TCP Packet******************************");
    ethernet_header(buffer,buflen);
    ip_header(buffer,buflen,&iphdrlen);
    tcp = (struct tcphdr*)(buffer + iphdrlen + sizeof(struct ethhdr));
    printf("\nTCP Header\n");
    printf("\t|-Source Port          : %u\n",ntohs(tcp->source));
    printf("\t|-Destination Port     : %u\n",ntohs(tcp->dest));
    printf("\t|-Sequence Number      : %u\n",ntohl(tcp->seq));
    printf("\t|-Acknowledge Number   : %u\n",ntohl(tcp->ack_seq));
    printf("\t|-Header Length        : %d DWORDS or %d BYTES\n",(unsigned int)tcp->doff,(unsigned int)tcp->doff*4);
    printf("\t|----------Flags-----------\n");
    printf("\t\t|-Urgent Flag          : %d\n",(unsigned int)tcp->urg);
    printf("\t\t|-Acknowledgement Flag : %d\n",(unsigned int)tcp->ack);
    printf("\t\t|-Push Flag            : %d\n",(unsigned int)tcp->psh);
    printf("\t\t|-Reset Flag           : %d\n",(unsigned int)tcp->rst);
    printf("\t\t|-Synchronise Flag     : %d\n",(unsigned int)tcp->syn);
    printf("\t\t|-Finish Flag          : %d\n",(unsigned int)tcp->fin);
    printf("\t|-Window size          : %d\n",ntohs(tcp->window));
    printf("\t|-Checksum             : %d\n",ntohs(tcp->check));
    printf("\t|-Urgent Pointer       : %d\n",tcp->urg_ptr);

    payload(buffer,buflen,&iphdrlen);

    printf("*****************************************************************\n\n\n");
}

/*Вывод udp заголовка*/
void udp_header(unsigned char* buffer, int buflen)
{
    int iphdrlen;
    struct udphdr *udp;
    printf("\n*************************UDP Packet******************************");
    ethernet_header(buffer,buflen);
    ip_header(buffer,buflen,&iphdrlen);
    printf("\nUDP Header\n");

    udp = (struct udphdr*)(buffer + iphdrlen + sizeof(struct ethhdr));
    printf("\t|-Source Port    	: %d\n", ntohs(udp->source));
    printf("\t|-Destination Port	: %d\n", ntohs(udp->dest));
    printf("\t|-UDP Length      	: %d\n", ntohs(udp->len));
    printf("\t|-UDP Checksum   	: %d\n", ntohs(udp->check));

    payload(buffer,buflen,&iphdrlen);

    printf("*****************************************************************\n\n\n");
}

/*Вывод всех заголовков и полезной нагрузки*/
void data_process(unsigned char* buffer,int buflen)
{
    struct iphdr *ip = (struct iphdr*)(buffer + sizeof (struct ethhdr));
    /*проверяем какой протокол указан в ip заголовке*/
    switch (ip->protocol)
    {
    case SOL_TCP:
        tcp_header(buffer,buflen);
        break;

    case SOL_UDP:
        udp_header(buffer,buflen);
        break;

    default:
        break;
    }

}