#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "my_show_packet.h"

/*Получение индекса интерфейса*/
void get_index(struct ifreq *ifreq_i,int sock_raw)
{
    memset(ifreq_i,0,sizeof(struct ifreq));
    strncpy(ifreq_i->ifr_name, DEFAULT_IF,IFNAMSIZ-1);

    if((ioctl(sock_raw,SIOCGIFINDEX,ifreq_i))<0)
        printf("error in index ioctl reading");

    printf("index=%d\n",ifreq_i->ifr_ifindex);
}

/*Создание сырого сокета*/
int create_raw_socket()
{
    int raw_socket;
    if ((raw_socket = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) == -1)
    {
        perror("listener: socket");
        exit(-1);
    }
    return raw_socket;
}

/*Биндинг к интерфейсу и установка опций сокета*/
void bind_and_setoptions(int sockfd,struct ifreq *if_idx)
{
    int sockopt=1;

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof sockopt) == -1)
    {
        perror("setsockopt");
        close(sockfd);
        exit(EXIT_FAILURE);
    }

    get_index(if_idx,sockfd);

    if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, DEFAULT_IF, IFNAMSIZ-1) == -1)
    {
        perror("SO_BINDTODEVICE");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
}

/*формирование эхо-ответа*/
void construction_echo_reply(unsigned char* buffer)
{
    char mac_addr_buff[ETH_ALEN];       //для переприсваивания mac адреса источника и получателя
    uint32_t ip_addr_buff;              //для переприсваивания ip адреса источника и получателя
    uint16_t upd_port_buff;             //для переприсваивания порта источника и получателя
    struct ether_header *eh = (struct ether_header *) buffer;
    struct iphdr *iph = (struct iphdr *) (buffer + sizeof(struct ether_header));
    struct udphdr *udph = (struct udphdr *) (buffer + sizeof(struct iphdr) + sizeof(struct ether_header));

    /*Замена mac адресов местами*/
    memcpy(mac_addr_buff,eh->ether_dhost, ETH_ALEN);
    memcpy(eh->ether_dhost, eh->ether_shost, ETH_ALEN);
    memcpy(eh->ether_shost, mac_addr_buff, ETH_ALEN);

    /*Замена ip адресов местами*/
    ip_addr_buff=iph->daddr;
    iph->daddr=iph->saddr;
    iph->saddr=ip_addr_buff;

    /*Замена портов местами*/
    upd_port_buff=udph->dest;
    udph->dest=udph->source;
    udph->source=upd_port_buff;

}

/*Отправка эхо-ответа*/
void send_echo_reply(int sockfd, unsigned char* buffer, int numbytes, struct sockaddr_ll* sock_address)
{
    printf("Sending echo-answer ...\n");
    
    if (sendto(sockfd, buffer, numbytes, 0, (struct sockaddr*)sock_address, sizeof(struct sockaddr_ll)) < 0)
        perror("Send failed\n");

    printf("Waiting new packet ...\n");
}

int main(int argc, char *argv[])
{
    int sockfd;                         //сокет
    ssize_t numbytes;                   //количество принятых байт
    struct sockaddr_ll sock_address;    //структура адреса физического уровня
    uint8_t buf[BUF_SIZ];               //буфер для приема пакетов
    struct ifreq if_idx;                //структура интерфейса
    unsigned int sizeaddr=sizeof(sock_address); //размер адреса sockaddr_ll

    /*Заголовок датаграмм*/
    struct udphdr *udph = (struct udphdr *) (buf + sizeof(struct iphdr) + sizeof(struct ether_header));
    
    memset(&sock_address,0,sizeof(struct sockaddr_ll));
    
    sockfd=create_raw_socket();

    bind_and_setoptions(sockfd,&if_idx);

    printf("Waiting to recvfrom...\n");

    while(1)
    {
        numbytes = recvfrom(sockfd, buf, BUF_SIZ, 0, (struct sockaddr*)&sock_address, &sizeaddr);

        /*Проверяем (фильтруем) порт назначения*/
        if (ntohs(udph->dest)==FILTRAGE_PORT)
        {
            /*Выводим все поля заголовков и полезную нагрузку*/
            data_process(buf,numbytes);

            /*Формируем эхо-ответ*/
            construction_echo_reply(buf);
            
            /*отправляем эхо-ответ*/
            send_echo_reply(sockfd,buf,numbytes,&sock_address);
        }
    }

    close(sockfd);
    
    return 0;
}
