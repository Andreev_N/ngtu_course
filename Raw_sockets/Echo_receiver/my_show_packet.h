#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#include <net/if.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/if_ether.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <linux/if_packet.h>
#include <arpa/inet.h>

#include <string.h>

/*Порт в tcp/upd по которому происходит фильтрация принятых пакетов*/
#define FILTRAGE_PORT 23451
/*Имя локального интерфейса*/
#define DEFAULT_IF "enp1s0"
/*Размер буфера для приема пакетов*/
#define BUF_SIZ 1024


/*Вывод заголовка ethernet кадра*/
void ethernet_header(unsigned char* buffer,int buflen);

/*Вывод ip заголовка*/
void ip_header(unsigned char* buffer,int buflen,int* iphdrlen);

/*Вывод полезной нагрузки*/
void payload(unsigned char* buffer,int buflen,int* iphdrlen);

/*Вывод tcp заголовка*/
void tcp_header(unsigned char* buffer,int buflen);

/*Вывод udp заголовка*/
void udp_header(unsigned char* buffer, int buflen);

/*Вывод всех заголовков и полезной нагрузки*/
void data_process(unsigned char* buffer,int buflen);
