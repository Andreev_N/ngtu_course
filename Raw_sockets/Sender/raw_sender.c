#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#include <net/if.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/if_ether.h>
#include <netinet/udp.h>
#include <linux/if_packet.h>
#include <arpa/inet.h>

/*МАС-адрес назначения*/
#define DESTMAC0	0x98
#define DESTMAC1	0x28
#define DESTMAC2	0xa6
#define DESTMAC3	0x0b
#define DESTMAC4	0x79
#define DESTMAC5	0x21

/*IP-адрес назначения*/
#define destination_ip "192.168.1.178"
/*Имя локального интерфейса*/
#define DEFAULT_IF "enp1s0"
/*Порты получателя и отправителя*/
#define DEST_PORT 23451
#define SOURCE_PORT 23452

#define MAX_PACKET_SIZE 1024
#define DATA "This is test string for my raw packet"

#define IP_ID 10201
#define IP_IHL 5

/*Получение индекса интерфейса*/
void get_index(struct ifreq *ifreq_i,int sock_raw)
{
    memset(ifreq_i,0,sizeof(struct ifreq));
    strncpy(ifreq_i->ifr_name, DEFAULT_IF,IFNAMSIZ-1);

    if((ioctl(sock_raw,SIOCGIFINDEX,ifreq_i))<0)
        printf("error in index ioctl reading");

    printf("index=%d\n",ifreq_i->ifr_ifindex);
}

/*Получение MAC-адреса интерфейса и заполнение заголовка кадра Ethernet*/
void get_mac(struct ifreq *ifreq_c,int sock_raw,unsigned char *sendbuff,int *total_len)
{
    struct ethhdr *eth = (struct ethhdr *)(sendbuff);
    
    memset(ifreq_c,0,sizeof(struct ifreq));
    strncpy(ifreq_c->ifr_name, DEFAULT_IF,IFNAMSIZ-1);

    if((ioctl(sock_raw,SIOCGIFHWADDR,ifreq_c))<0)
        printf("error in SIOCGIFHWADDR ioctl reading");

    printf("Mac= %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n",
           (unsigned char)(ifreq_c->ifr_hwaddr.sa_data[0]),
           (unsigned char)(ifreq_c->ifr_hwaddr.sa_data[1]),
           (unsigned char)(ifreq_c->ifr_hwaddr.sa_data[2]),
           (unsigned char)(ifreq_c->ifr_hwaddr.sa_data[3]),
           (unsigned char)(ifreq_c->ifr_hwaddr.sa_data[4]),
           (unsigned char)(ifreq_c->ifr_hwaddr.sa_data[5]));

    printf("packaging start ... \n");

    /*заполнение MAC-адреса источника*/
    memcpy(eth->h_source,ifreq_c->ifr_hwaddr.sa_data,ETH_ALEN);

    /*заполнение MAC-адреса получателя*/
    eth->h_dest[0] = DESTMAC0;
    eth->h_dest[1] = DESTMAC1;
    eth->h_dest[2] = DESTMAC2;
    eth->h_dest[3] = DESTMAC3;
    eth->h_dest[4] = DESTMAC4;
    eth->h_dest[5] = DESTMAC5;

    eth->h_proto = htons(ETH_P_IP);   //0x0800

    *total_len+=sizeof(struct ethhdr);
}

/*Заполнение поля информации*/
void get_data(unsigned char *sendbuff,int *total_len)
{
    memcpy(&sendbuff[*total_len],DATA,sizeof(DATA));
    *total_len+=sizeof(DATA);
}

/*Заполнение udp заголовка*/
void get_udp(unsigned char *sendbuff,int *total_len)
{
    struct udphdr *uh = (struct udphdr *)(sendbuff + sizeof(struct iphdr) + sizeof(struct ethhdr));

    uh->source = htons(SOURCE_PORT);
    uh->dest = htons(DEST_PORT);
    uh->check = 0;

    *total_len+= sizeof(struct udphdr);
    get_data(sendbuff,total_len);
    uh->len = htons((*total_len - sizeof(struct iphdr) - sizeof(struct ethhdr)));
}

/*Подсчет контрольной суммы*/
unsigned short checksum(unsigned short* buff, int _16bitword)
{
    unsigned long sum;
    for(sum=0; _16bitword>0; _16bitword--)
        sum+=htons(*(buff)++);
    do
    {
        sum = ((sum >> 16) + (sum & 0xFFFF));
    }
    while(sum & 0xFFFF0000);

    return (~sum);
}

/*Заполнение ip заголовка*/
void get_ip(struct ifreq *ifreq_ip,int sock_raw,unsigned char *sendbuff,int *total_len)
{
    struct iphdr *iph = (struct iphdr*)(sendbuff + sizeof(struct ethhdr));
    
    memset(ifreq_ip,0,sizeof(struct ifreq));
    strncpy(ifreq_ip->ifr_name, DEFAULT_IF,IFNAMSIZ-1);
    if(ioctl(sock_raw,SIOCGIFADDR,ifreq_ip)<0)
    {
        printf("error in SIOCGIFADDR \n");
    }

    printf("%s\n",inet_ntoa((((struct sockaddr_in*)&(ifreq_ip->ifr_addr))->sin_addr)));

    iph->version = IPVERSION;
    iph->ihl = IP_IHL;
    iph->tos = IPTOS_LOWDELAY;
    iph->id = htons(IP_ID);
    iph->ttl = IPDEFTTL;
    iph->protocol = SOL_UDP;
    iph->saddr = inet_addr(inet_ntoa((((struct sockaddr_in *)&(ifreq_ip->ifr_addr))->sin_addr)));
    iph->daddr = inet_addr(destination_ip);
    *total_len += sizeof(struct iphdr);
    get_udp(sendbuff,total_len);

    iph->tot_len = htons(*total_len - sizeof(struct ethhdr));
    iph->check = htons(checksum((unsigned short*)(sendbuff + sizeof(struct ethhdr)), (sizeof(struct iphdr)/2)));
}

/*создание сырого сокета*/
int create_raw_socket()
{
    int raw_socket;
    raw_socket=socket(AF_PACKET,SOCK_RAW,IPPROTO_RAW);
    if(raw_socket == -1)
        printf("error in socket");
    return raw_socket;
}

/*заполнение структуры struct sockaddr_ll*/
void fill_sockaddr_ll(struct sockaddr_ll *sadr_ll,struct ifreq *ifreq_i)
{
    memset(sadr_ll,0,sizeof(struct sockaddr_ll));

    sadr_ll->sll_ifindex = ifreq_i->ifr_ifindex;
    sadr_ll->sll_halen   = ETH_ALEN;
    sadr_ll->sll_addr[0]  = DESTMAC0;
    sadr_ll->sll_addr[1]  = DESTMAC1;
    sadr_ll->sll_addr[2]  = DESTMAC2;
    sadr_ll->sll_addr[3]  = DESTMAC3;
    sadr_ll->sll_addr[4]  = DESTMAC4;
    sadr_ll->sll_addr[5]  = DESTMAC5;
}

void send_my_packet(int sock_raw, unsigned char *sendbuff,int total_len,struct sockaddr_ll sadr_ll)
{
    int send_len;                                   //количество отправленных байт
    printf("sending...\n");
    
    send_len = sendto(sock_raw,sendbuff,total_len,0,(struct sockaddr*)&sadr_ll,sizeof(struct sockaddr_ll));
    if(send_len<0)
    {
        printf("error in sending....sendlen=%d....errno=%d\n",send_len,errno);
        exit(-1);
    }
}


int main(int argc, char *argv[])
{
    int total_len=0;                                //длина формируемого пакета    
    struct ifreq ifreq_c,ifreq_i,ifreq_ip;          //структуры ifreq (разные для каждого ioctl)
    int sock_raw;                                   //сырой сокет
    unsigned char sendbuff[MAX_PACKET_SIZE];        //формируемый пакет
    struct sockaddr_ll sadr_ll;                     //структура адреса физического уровня

    memset(sendbuff,0,MAX_PACKET_SIZE);

    sock_raw=create_raw_socket();

    get_index(&ifreq_i,sock_raw);
    get_mac(&ifreq_c,sock_raw,sendbuff,&total_len);
    get_ip(&ifreq_ip,sock_raw,sendbuff,&total_len);  

    fill_sockaddr_ll(&sadr_ll,&ifreq_i);
    
    send_my_packet(sock_raw,sendbuff,total_len,sadr_ll);
    
    close(sock_raw);

    return 0;
}

