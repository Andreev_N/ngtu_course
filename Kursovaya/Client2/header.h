#define MAX_STRING_LEN 15
#define TIME_FOR_SLEEP 7
#define MAX_SLEEP_TIME 1000

struct protocol
{
    int time;
    int len;
    char* text;
};

struct udp_alarm
{
    int type_client;
    int serv_port;
};

