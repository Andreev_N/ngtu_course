#include <stdio.h>

#include "header.h"

void dieWithError(char* errorMessage)
{
    perror(errorMessage);
    exit(1);
}

void recv_msg(int clntSocket, int msgqid)
{
    int recvMsgSize;           //размер принятого сообщения
    struct mymsg_buf qbuf;     //структура сообщения очереди
    struct protocol proto;     //структура клиентского сообщения
    struct msqid_ds queuestat; //структура для параметров очереди

    memset(&qbuf, 0, sizeof(struct mymsg_buf));
    memset(&proto, 0, sizeof(struct protocol));

    /*формирование сообщения для очереди*/
    qbuf.mtype = MSGTYPE;

    /*прием сообщения клиента 1го типа*/
    if((recvMsgSize = recv(clntSocket, &qbuf.time, sizeof(int) * 2, 0)) < 0)
        dieWithError("recv() failed");

    if((recvMsgSize = recv(clntSocket, qbuf.text, qbuf.len, 0)) < 0)
    {
        dieWithError("recv() failed");
    }

    close(clntSocket);

    printf("Get message from client [1]: time:[%d]; lenght:[%d]; text:[%s]\n", qbuf.time, qbuf.len, qbuf.text);

    /*размер сообщения очереди не учитывая mtype*/
    recvMsgSize = 2 * sizeof(int) + qbuf.len;

    msgctl(msgqid, IPC_STAT, &queuestat); //необходимо извлечь количество сообщений в очереди
    if(queuestat.msg_qnum < MAX_QUEUE_SIZE) //проверка на ограничение очереди
    {
        /*если ограничение не достигнуто, помещаем сообщение в очередь*/
        if((msgsnd(msgqid, (struct msgbuf*)&qbuf, recvMsgSize, 0)) == -1)
        {
            perror("msgsnd");
            exit(1);
        }
        printf("Queue size: [%ld]\n", queuestat.msg_qnum+1);
    }

}

void send_msg(int clntSocket, int msgqid)
{
    int sendMsgSize; //количество отправленных байт
    struct mymsg_buf qbuf;
    struct msqid_ds queuestat;
    int byt; //для проверки результатов

    memset(&qbuf, 0, sizeof(struct mymsg_buf));

    msgctl(msgqid, IPC_STAT, &queuestat);
    if(queuestat.msg_qnum > 0)
    {
        sendMsgSize = msgrcv(msgqid, &qbuf, sizeof(qbuf)-sizeof(qbuf.mtype), MSGTYPE, 0);
        if(sendMsgSize < 0)
        {
            perror("msgrecv");
            exit(1);
        }
        printf("Msg from QUEUE: time:[%d]; lenght:[%d]; text:[%s] send to client [2]\n", qbuf.time, qbuf.len,
           qbuf.text);
        printf("Queue size: [%ld]\n", queuestat.msg_qnum-1);
    }

    sendMsgSize = 2 * sizeof(int);
    byt = send(clntSocket, &qbuf.time, sendMsgSize, 0);
    if(byt < 0)
        dieWithError("send() failed");

    byt = send(clntSocket, qbuf.text, qbuf.len, 0);
    if(byt < 0)
        dieWithError("send() failed");

    close(clntSocket);

}
void* tcp_recv(void* arg)
{
    Data* thread_data = (Data*)arg; //переданные параметры
    int servSock;                   //сокет сервера
    int clntSock;                   //клиентский сокет
    struct sockaddr_in servAddr;    //локальный адрес сервера
    struct sockaddr_in clntAddr;    // адрес клиента
    unsigned int clntLen;           //длина адреса
    int reuse = 1;                  //для параметра SO_REUSEADDR

    /* создание сокета */
    if((servSock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        dieWithError("socket() failed");

    /* заполение структуры адреса */
    memset(&servAddr, 0, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(thread_data->port);

    /*разрешаем повторное использование адреса*/
    if(setsockopt(servSock, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0)
        dieWithError("failed allowing server socket to reuse address");

    /* биндинг сокета */
    if(bind(servSock, (struct sockaddr*)&servAddr, sizeof(servAddr)) < 0)
        dieWithError("bind() failed");

    /* слушаем сокет */
    if(listen(servSock, MAXPENDING) < 0)
        dieWithError("listen() failed");

    for(;;)
    {
        clntLen = sizeof(clntAddr);

        if((clntSock = accept(servSock, (struct sockaddr*)&clntAddr, &clntLen)) < 0)
            dieWithError("accept() failed");

        /*на следующем участке поток не прерывается пока не завершит отправку*/
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
        recv_msg(clntSock, thread_data->msgqid);
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    }
}

void* tcp_send(void* arg)
{
    Data* thread_data = (Data*)arg;
    int servSock;
    int clntSock;
    struct sockaddr_in echoServAddr;
    struct sockaddr_in echoClntAddr;
    unsigned int clntLen;
    int reuse = 1;

    if((servSock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        dieWithError("socket() failed");

    if(setsockopt(servSock, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0)
        dieWithError("failed allowing server socket to reuse address");

    memset(&echoServAddr, 0, sizeof(echoServAddr));
    echoServAddr.sin_family = AF_INET;
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    /* порт для отправки сообщений клиентам 2го типа выбирается следующим после введенного параметром */
    echoServAddr.sin_port = htons(TCP_PORT_SEND);

    if(bind(servSock, (struct sockaddr*)&echoServAddr, sizeof(echoServAddr)) < 0)
        dieWithError("bind() failed");

    if(listen(servSock, MAXPENDING) < 0)
        dieWithError("listen() failed");

    for(;;)
    {

        clntLen = sizeof(echoClntAddr);

        if((clntSock = accept(servSock, (struct sockaddr*)&echoClntAddr, &clntLen)) < 0)
            dieWithError("accept() failed");

        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
        send_msg(clntSock, thread_data->msgqid);
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    }
}

void* udp_first_clients(void* arg)
{
    Data* thread_data = (Data*)arg;
    int clntSock;
    struct sockaddr_in clntAddr;
    struct udp_alarm send_msg;
    struct msqid_ds queuestat;
    int client_port = CLIENT1_PORT_MIN;
    int range = CLIENT1_PORT_MAX - CLIENT1_PORT_MIN; //диапазон портов

    /*формирование сообщения для оповещений*/
    send_msg.type_client = 1;
    send_msg.serv_port = TCP_PORT_RECV;

    if((clntSock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        dieWithError("socket() failed");

    memset(&clntAddr, 0, sizeof(clntAddr));
    clntAddr.sin_family = AF_INET;
    clntAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    for(;;)
    {
        client_port = CLIENT1_PORT_MIN;
        /*Проверяем есть ли свободные места в очереди*/
        msgctl(thread_data->msgqid, IPC_STAT, &queuestat); //необходимо извлечь количество сообщений в очереди
        if(queuestat.msg_qnum < MAX_QUEUE_SIZE) //проверка на ограничение очереди
        {
            //если есть то отправляем запросы на указанный ранее диапазон портов
            for(int i = 0; i < range; i++)
            {
                clntAddr.sin_port = htons(client_port);
                if(sendto(clntSock, &send_msg, sizeof(struct udp_alarm), 0, (struct sockaddr*)&clntAddr,
                          sizeof(clntAddr)) != sizeof(struct udp_alarm))
                    dieWithError("sendto() sent a different number of bytes than expected");
                client_port++;
            }
        }

        sleep(SLEEP_TIME);
    }
}

void* udp_second_clients(void* arg)
{
    Data* thread_data = (Data*)arg;
    int clntSock;
    struct sockaddr_in clntAddr;
    struct udp_alarm send_msg;
    struct msqid_ds queuestat;
    int client_port = CLIENT2_PORT_MIN;
    int range = CLIENT2_PORT_MAX - CLIENT2_PORT_MIN;

    send_msg.type_client = 2;
    send_msg.serv_port = TCP_PORT_SEND;

    if((clntSock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        dieWithError("socket() failed");

    memset(&clntAddr, 0, sizeof(clntAddr));
    clntAddr.sin_family = AF_INET;
    clntAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    for(;;)
    {
        client_port = CLIENT2_PORT_MIN;

        msgctl(thread_data->msgqid, IPC_STAT, &queuestat); //необходимо извлечь количество сообщений в очереди
        if(queuestat.msg_qnum > 0) //проверка на ограничение очереди
        {
            for(int i = 0; i < range; i++)
            {
                clntAddr.sin_port = htons(client_port); /* Local port */
                if(sendto(clntSock, &send_msg, sizeof(struct udp_alarm), 0, (struct sockaddr*)&clntAddr,
                          sizeof(clntAddr)) != sizeof(struct udp_alarm))
                    dieWithError("sendto() sent a different number of bytes than expected");

                client_port++;
            }
        }

        sleep(SLEEP_TIME);
    }
}

void create_threads(pthread_t* thread_tcp_recv,
                    pthread_t* thread_tcp_send,
                    pthread_t* thread_udp_first,
                    pthread_t* thread_udp_second,
                    Data* thread_data)
{
    int result;
    result = pthread_create(thread_tcp_recv, NULL, tcp_recv, thread_data);
    if(result != 0)
    {
        perror("Creating the first TCP thread");
        exit(EXIT_FAILURE);
    }

    result = pthread_create(thread_tcp_send, NULL, tcp_send, thread_data);
    if(result != 0)
    {
        perror("Creating the second TCP thread");
        exit(EXIT_FAILURE);
    }

    result = pthread_create(thread_udp_first, NULL, udp_first_clients, thread_data);
    if(result != 0)
    {
        perror("Creating the first UDP thread");
        exit(EXIT_FAILURE);
    }

    result = pthread_create(thread_udp_second, NULL, udp_second_clients, thread_data);
    if(result != 0)
    {
        perror("Creating the second UDP thread");
        exit(EXIT_FAILURE);
    }
}

void join_all_threads(pthread_t tcp_first, pthread_t tcp_second, pthread_t udp_first, pthread_t udp_second)
{
    int result;
    result = pthread_join(tcp_first, NULL);
    if(result != 0)
    {
        perror("Joining the first TCP thread");
        exit(EXIT_FAILURE);
    }

    result = pthread_join(tcp_second, NULL);
    if(result != 0)
    {
        perror("Joining the second TCP thread");
        exit(EXIT_FAILURE);
    }

    result = pthread_join(udp_first, NULL);
    if(result != 0)
    {
        perror("Joining the first UDP thread");
        exit(EXIT_FAILURE);
    }

    result = pthread_join(udp_second, NULL);
    if(result != 0)
    {
        perror("Joining the second UDP thread");
        exit(EXIT_FAILURE);
    }
}
