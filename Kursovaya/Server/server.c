#include <stdio.h>

#include "header.h"

pthread_t thread_tcp_recv;      //прием сообщения у клиента 1го типа
pthread_t thread_tcp_send;      //отправка сообщения клиенту 2го типа
pthread_t thread_udp_first;     //отправка оповещений клиентам 1го типа
pthread_t thread_udp_second;    //отправка оповещений клиентам 2го типа

/*обработчик сигнала SIGINT - запрос всем потокам на завершение*/
void sigint()
{
    pthread_cancel(thread_tcp_recv);
    pthread_cancel(thread_tcp_send);
    pthread_cancel(thread_udp_first);
    pthread_cancel(thread_udp_second);
    printf("The threads is stopped.\n");
}

int main(int argc, char* argv[])
{
    int msgqid;                     //идентификатор очереди
    Data thread_data;               //данные для потоков

    signal(SIGINT, sigint);

    /*создание очереди, если её не существует*/
    msgqid = msgget(MSGKEY, MSGPERM | IPC_CREAT);
    if(msgqid < 0)
    {
        printf("failed to create message queue\n");
        exit(1);
    }

    /*инициализация данных для потоков*/
    thread_data.port = TCP_PORT_RECV;
    thread_data.msgqid = msgqid;

    /*создание 4х потоков*/
    create_threads(&thread_tcp_recv, &thread_tcp_send, &thread_udp_first, &thread_udp_second,&thread_data);

    /*присоединение всех потоков (после Сtrl+C)*/
    join_all_threads(thread_tcp_recv, thread_tcp_send,thread_udp_first, thread_udp_second);

    printf("exit\n");

    return 0;
}
