#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

#define SIZE_BUF 10

int main(int argc, char* argv[])
{
    int count_process; //число процессов
    FILE* file;
    int* pid;           //для массива PIDов
    char buf[SIZE_BUF]; //для конвертации из чисел в строку через sprintf
    int status, stat;   //статус и возвращаемое значение для waitpid

    printf("Enter number of process\n");
    scanf("%d", &count_process);
    printf("The hunger games is begin! \n");

    pid = (int*)malloc(sizeof(int) * (count_process + 1));

    for(int i = 1; i <= count_process; i++)
    {
        pid[i] = fork();
        if(pid[i] == 0)
        {
            execl("./process", "process", NULL);
        }
    }

    //открываем(создаем) файл для записи PIDов
    if((file = fopen("Pids.txt", "w+")) == NULL)
    {
        printf("Не удается открыть файл.\n");
        exit(2);
    }

    printf("Pretenders:\n");

    for(int i = 1; i <= count_process; i++)
    {
        sprintf(buf, "%d", pid[i]);
        printf("Pid #%d - [%d]\n", i, pid[i]);
        fputs(buf, file);
        fputc('\n', file);
    }
    fclose(file);

    //ждем завершение всех процессов
    for(int i = 1; i <= count_process; i++)
    {
        status = waitpid(pid[i], &stat, 0);
        if(pid[i] == status)
        {

            printf("Parent: process [%d] done,  result=%d (0-dead, 1-win)\n", pid[i], WEXITSTATUS(stat));
        }
    }
    
    // Открываем файл, в которой остался только PID победителя
    if((file = fopen("Pids.txt", "r")) == NULL)
    {
        printf("Не удается открыть файл.\n");
        exit(2);
    }
    
    fgets(buf, SIZE_BUF, file);
    printf("-----------------------------\n");
    printf("Winner is PID number - [%d]\n", atoi(buf));
    printf("-----------------------------\n");

    free(pid);
    fclose(file);
    return 0;
}
