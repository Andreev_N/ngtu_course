#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define SIZE_BUF 10

int main(int argc, char* argv[])
{
    int random_process;               //номер рандомного процесса
    char buf[SIZE_BUF] = "";          //буфер для чтения
    char pid_for_kill[SIZE_BUF] = ""; //выбранный случайный PID
    int count_str = 0;
    FILE* f;
    FILE* temp; //для изменения первого файла
    int my_pid = getpid();

    memset(buf, 0, SIZE_BUF);

    srand(getpid());

    while(count_str != 1) //пока не останется один
    {

        sleep(3 + rand() % 15); //спим случайное время от 3 до 18 сек и идем убивать

        if((f = fopen("Pids.txt", "r")) == NULL)
        {
            printf("Не удается открыть файл.\n");
            exit(2);
        }

        count_str = 0;
        //подстчет строк(количества живых процессов)
        while(fgets(buf, SIZE_BUF, f) != NULL)
        {
            count_str++;
        }

        //если он последний
        if(count_str == 1)
        {
            printf("Im [%d] the last one.\n", my_pid);
            fclose(f);
            exit(1);
        }

        do //выполняем, пока не выбрали ЧУЖОЙ PID
        {
            random_process = 1 + rand() % (count_str);
            fseek(f, 0, SEEK_SET);

            for(int i = 0; i < random_process; i++)
            {
                fgets(buf, SIZE_BUF, f);
            }

        } while(atoi(buf) == my_pid);

        //запоминаем найденный PID для kill
        strcpy(pid_for_kill, buf);

        if((temp = fopen("temp.txt", "w")) == NULL)
        {
            printf("Не удается открыть файл.\n");
            exit(3);
        }

        //удаление записи убитого процесса из файла
        fseek(f, 0, SEEK_SET);
        while(fgets(buf, SIZE_BUF, f) != NULL)
        {
            if(strstr(pid_for_kill, buf) == 0)
                fputs(buf, temp);
        }

        fclose(f);
        fclose(temp);

        remove("Pids.txt");
        rename("temp.txt", "Pids.txt");

        kill(atoi(pid_for_kill), SIGINT); //отправка сигнала на завершение выбранного процесса

        printf("Сhild: process [%d] choose PID %d for kill\n", my_pid, atoi(pid_for_kill));
    }

    return 0;
}
