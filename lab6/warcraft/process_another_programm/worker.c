#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{ 
    pid_t parent_pid;
    int random_time;
    
     if (argc<2) {
        printf("Usage: worker number\n");
        exit(-1);
    }
    
    parent_pid=getppid();
    srand(getpid());
    
    
    while(1)
    {
    random_time=5+rand()%11; // от 5 до 15 секунд
    sleep(random_time);    
    kill(parent_pid,SIGUSR1); 
    printf("Worker #%d take 10 gold\n",atoi(argv[1]));
    }
    return 0;
}
