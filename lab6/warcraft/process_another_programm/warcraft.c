#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#define SIZE_BUF_FOR_SPRINTF 2

int max_gold;

void gold_farming(int signum) //замена действий сигнала
{
    signal(SIGUSR1, gold_farming); //чтобы не сбрасывался после первой замены
    max_gold=max_gold-10;
    if (max_gold>0)
    printf("Gold in mine: %d\n",max_gold); //показывать текущее золото
}

int main(int argc, char* argv[], char* env[])
{
    int count_workers;
    int* pid;
    char buf[SIZE_BUF_FOR_SPRINTF];  //для конвертации из чисел в строку через sprintf
    
     if( signal(SIGUSR1, gold_farming) == SIG_ERR  )
    {
        printf("Parent: Unable to create handler for SIGUSR1\n");
    }
    printf("Enter max gold in mine\n");
    scanf("%d", &max_gold);
    printf("Enter number of workers\n");
    scanf("%d", &count_workers);
    printf("Work is started! Gold in mine:%d\n",max_gold);
    
    pid = (int*)malloc(sizeof(int) * count_workers);

    for(int i = 1; i <= count_workers; i++)
    {
        pid[i] = fork();
         
        if(pid[i] == 0)
        {           
            sprintf(buf, "%d", i);
            if(execl("./worker", "worker", buf, NULL) < 0)
            {
                printf("ERROR create worker %d\n", i);
                exit(-1);
            }            
            exit(1);
        }
    }

    while (1)
    {
        if (max_gold<=0)
        {
            for(int i = 1; i <= count_workers; i++)
            {
                kill(pid[i],SIGKILL);
            }
            printf("Mine is empty!\n");
            break;
        }
    }            
    
    free(pid);
    return 0;
}
