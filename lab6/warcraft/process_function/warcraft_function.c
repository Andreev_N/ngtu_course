#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int max_gold;

void worker(int worker_number)
{   
    pid_t parent_pid;
    int random_time;
    
    parent_pid=getppid();
    srand(getpid());  
    
    while(1)
    {
    random_time=5+rand()%6; // от 5 до 10 секунд
    sleep(random_time);    
    kill(parent_pid,SIGUSR1); 
    printf("Worker #%d take 10 gold\n",worker_number);
    }
}

void gold_farming(int signum)
{
    signal(SIGUSR1, gold_farming);
    max_gold=max_gold-10;
    printf("Gold in mine: %d\n",max_gold);
}

int main(int argc, char* argv[], char* env[])
{
    int count_workers;
    int* pid;

    signal(SIGUSR1, gold_farming);
    
    printf("Enter max gold in mine\n");
    scanf("%d", &max_gold);
    printf("Enter number of workers\n");
    scanf("%d", &count_workers);
    printf("Work is started! Gold in mine:%d\n",max_gold);
    
    pid = (int*)malloc(sizeof(int) * count_workers);

    for(int i = 1; i <= count_workers; i++)
    {
        pid[i] = fork();
         
        if(pid[i] == 0)
        {
            worker(i);
            exit(1);
        }
    }

    while (1)
    {
        if (max_gold<=0)
        {
            for(int i = 1; i <= count_workers; i++)
            {
                kill(pid[i],SIGKILL);
            }
            printf("Mine is empty!\n");
            break;
        }
    }    

    free(pid);
    return 0;
}
