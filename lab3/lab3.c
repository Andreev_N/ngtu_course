#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE_LASTNAME 50
#define NUMBERS_OF_EMPLOYEES 3

typedef struct employee
{
    char lastname[MAX_SIZE_LASTNAME];
    short year_of_birth;
    short department_num;
    int salary;
} employees;

void read_employees(employees** em, int number_emp)
{

    for(int i = 0; i < number_emp; i++)
    {
        em[i] = (employees*)malloc(sizeof(employees));
        printf("%d EMPLOYEE:\n", i + 1);
        printf("enter lastname:");
        scanf("%s", em[i]->lastname);
        printf("enter year of birth:");
        scanf("%hi", &(em[i])->year_of_birth);
        printf("enter department number:");
        scanf("%hi", &em[i]->department_num);
        printf("enter salary:");
        scanf("%d", &em[i]->salary);
    }
}

void print_employees(employees** em, int number_emp)
{
    for(int i = 0; i < number_emp; i++)
    {
        printf("\nEMPLOYEE №%d:\n", i + 1);
        printf("---------------\n");
        printf("lastname:%s\n", em[i]->lastname);
        printf("year of birth:%hi\n", em[i]->year_of_birth);
        printf("department number:%hi\n", em[i]->department_num);
        printf("salary:%d\n", em[i]->salary);
        printf("---------------\n");
    }
}

static int cmp(const void* p1, const void* p2)
{
    employees* st1 = *(employees**)p1;
    employees* st2 = *(employees**)p2;
    return st1->year_of_birth - st2->year_of_birth;
}

void freeMem(employees** em, int count)
{
    for(int i = 0; i < count; i++)
    {
        free(em[i]);
    }
    free(em);
}

int main(int argc, char* argv[])
{
    employees** emp = (employees**)malloc(sizeof(employees**) * NUMBERS_OF_EMPLOYEES);
    read_employees(emp, NUMBERS_OF_EMPLOYEES); 
    qsort(emp, NUMBERS_OF_EMPLOYEES, sizeof(employees*), cmp);
    print_employees(emp, NUMBERS_OF_EMPLOYEES);
    freeMem(emp, NUMBERS_OF_EMPLOYEES);

    return (0);
}