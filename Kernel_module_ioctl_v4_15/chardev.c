#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/version.h>
#include <linux/uaccess.h>

#include "chardev.h"

#define SUCCESS 0
#define DEVICE_NAME "char_dev"

/*для индикации открыто ли устройство*/
static int Device_Open = 0;

/*для сообщений устройства*/
static char Message[MAX_MSG_SIZE];

/*Позиция в буфере.Используется в том случае, если сообщение оказывется длиннее
чем размер буфера.*/
static char *Message_Ptr;

/*открытие файла устройства процессом*/
static int device_open(struct inode *inode, struct file *file)
{
#ifdef DEBUG
    printk("device_open(%p)\n", file);
#endif
    /*В каждый конкретный момент времени только один процесс может открыть файл устройства */
    if (Device_Open)
        return -EBUSY;

    Device_Open++;
    /*Инициализация сообщения*/
    Message_Ptr = Message;
    try_module_get(THIS_MODULE);
    return SUCCESS;
}

/*закрытие файла устройства процессом*/
static int device_release(struct inode *inode, struct file *file)
{
#ifdef DEBUG
    printk("device_release(%p,%p)\n", inode, file);
#endif

    Device_Open--;

    module_put(THIS_MODULE);
    return SUCCESS;
}

/*чтение процессом данных из файла устройства*/
static ssize_t device_read(struct file *file, char __user * buffer, size_t length, loff_t * offset)
{
    /*Количество байт, фактически записанных в буфер*/
    int bytes_read = 0;

#ifdef DEBUG
    printk("device_read(%p,%p,%d)\n", file, buffer, length);
#endif

    /*Если достигнут конец сообщения -- вернуть 0(признак конца файла)*/
    if (*Message_Ptr == 0)
        return 0;

    /*Запись данных в буфер*/
    while (length && *Message_Ptr)
    {
        put_user(*(Message_Ptr++), buffer++);
        length--;
        bytes_read++;
    }

#ifdef DEBUG
    printk("Read %d bytes, %d left\n", bytes_read, length);
#endif

    /* Вернуть количество байт, помещенных в буфер*/
    return bytes_read;
}

/*заппись процессом данных в файл устройства*/
static ssize_t device_write(struct file *file, const char __user * buffer, size_t length, loff_t * offset)
{
    int i;

    memset(Message,0,MAX_MSG_SIZE-1);

#ifdef DEBUG
    printk("device_write(%p,%s,%d)", file, buffer, length);
#endif

    for (i = 0; i < length && i < MAX_MSG_SIZE; i++)
        get_user(Message[i], buffer + i);

    Message_Ptr = Message;

    /*Вернуть количество принятых байт*/
    return i;
}

/* Операция ioctl над файлом устройства */
long device_ioctl(struct file *file, unsigned int ioctl_num, unsigned long ioctl_param)
{
    int i;
    char *temp;
    char ch;

    /* Реакция на различные команды ioctl */
    switch (ioctl_num)
    {
    case IOCTL_SET_MSG:
        /*Принять указатель на сообщение (в пространстве пользователя)и переписать в буфер,
        aдрес которого задан в дополнительно аргументе.*/
        temp = (char *)ioctl_param;

        /*Находим длину сообщения*/
        get_user(ch, temp);
        for (i = 0; ch && i < MAX_MSG_SIZE; i++, temp++)
            get_user(ch, temp);
        /*записываем в файл устройства*/
        device_write(file, (char *)ioctl_param, i, 0);
        break;

    case IOCTL_GET_MSG:
        /* Передать текущее сообщение вызывающему процессу -
        записать по указанному адресу */
        i = device_read(file, (char *)ioctl_param, MAX_MSG_SIZE-1, 0);

        /*Вставить в буфер завершающий символ \0*/
        put_user('\0', (char *)ioctl_param + i);
        break;

    case IOCTL_GET_NTH_BYTE:
        /*Этот вызов является вводом (ioctl_param) и
        выводом (возвращаемое значение функции) одновременно*/
        return Message[ioctl_param];
        break;
    }

    return SUCCESS;
}


/*Структура адресов функций-обработчиков*/
struct file_operations Fops =
{
    .read = device_read,
    .write = device_write,
    .open = device_open,
    .release = device_release,
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
    .ioctl = device_ioctl, //для версий поздних 2.6.35 поле ioctl удалено
#else
    .unlocked_ioctl = device_ioctl,
#endif
};

/*Инициализация модуля - Регистрация символьного устройства */
int init_module()
{
    int ret_val;
    /*Попытка регистрации символьного устройства */
    ret_val = register_chrdev(MAJOR_NUM, DEVICE_NAME, &Fops);

    if (ret_val < 0)
    {
        printk("%s failed with %d\n",
               "Sorry, registering the character device ", ret_val);
        return ret_val;
    }

    printk("%s The major device number is %d.\n",
           "Registeration is a success", MAJOR_NUM);
    printk("Use:\n");
    printk("mknod %s c %d 0\n", DEVICE_FILE_NAME, MAJOR_NUM);

    return 0;
}

/*Завершение работы модуля */
void cleanup_module()
{
    unregister_chrdev(MAJOR_NUM, DEVICE_NAME);
}
