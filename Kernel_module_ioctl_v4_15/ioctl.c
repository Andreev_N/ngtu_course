#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "chardev.h" //Определения старшего номера устройства и коды операций ioctl

/*функция ioctl для записи сообщения в файл устройства*/
void ioctl_set_msg(int file_desc, char *message)
{
    int ret_val;

    ret_val = ioctl(file_desc, IOCTL_SET_MSG, message);

    if (ret_val < 0)
    {
        printf("Ошибка при вызове ioctl_set_msg: %d\n", ret_val);
        exit(-1);
    }
    else
        printf("Запись строки при вызове ioctl_set_msg: %s\n", message);

}

/*функция ioctl для получения сообщения из файла устройства*/
void ioctl_get_msg(int file_desc)
{
    int ret_val;
    char message[MAX_MSG_SIZE];

    ret_val = ioctl(file_desc, IOCTL_GET_MSG, message);

    if (ret_val < 0)
    {
        printf("Ошибка при вызове ioctl_get_msg: %d\n", ret_val);
        exit(-1);
    }

    printf("Получено сообщение (get_msg): %s\n", message);
}

/*функция ioctl для побайтного получения сообщения из файла устройство*/
void ioctl_get_nth_byte(int file_desc)
{
    int i;
    char c=1; //ненулевое значение, чтобы войти в цикл while

    printf("N-ный байт в сообщении (get_nth_byte): ");

    i = 0;
    while (c != 0)
    {
        c = ioctl(file_desc, IOCTL_GET_NTH_BYTE, i++);

        if (c < 0)
        {
            printf
            ("Ошибка при вызове ioctl_get_nth_byte на %d-м байте.\n", i);
            exit(-1);
        }

        putchar(c);
    }
    putchar('\n');
}

int main(int argc, char** argv)
{
    int file_desc, ret_val;
    char *msg = "my_ioctl_string\n"; //строка для записи в устройство

    file_desc = open(DEVICE_FILE_NAME, 0);
    if (file_desc < 0)
    {
        printf("Невозможно открыть файл устройства: %s\n", DEVICE_FILE_NAME);
        exit(-1);
    }

    ioctl_get_nth_byte(file_desc);
    ioctl_get_msg(file_desc);
    ioctl_set_msg(file_desc, msg);

    close(file_desc);
}
