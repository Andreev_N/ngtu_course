#ifndef CHARDEV_H
#define CHARDEV_H

#include <linux/ioctl.h>
 
/*Старший номер устройства. В случае использования ioctl,
мы уже лишены возможности воспользоваться динамическим номером,
поскольку он должен быть известен заранее.*/
#define MAJOR_NUM 100

#define MAX_MSG_SIZE 100
 
/*Операция передачи сообщения драйверу устройства*/
#define IOCTL_SET_MSG _IOR(MAJOR_NUM, 0, char *)
/*_IOR означает, что команда передает данные
от пользовательского процесса к модулю ядра
Первый аргумент, MAJOR_NUM -- старший номер устройства.
Второй аргумент -- код команды
(можно указать иное значение).
Третий аргумент -- тип данных, передаваемых в ядро*/

/*Операция получения сообщения от драйвера устройства */
#define IOCTL_GET_MSG _IOR(MAJOR_NUM, 1, char *)

/*Операция побайтного получения сообщения от драйвера устройства*/
#define IOCTL_GET_NTH_BYTE _IOWR(MAJOR_NUM, 2, int)

/*Имя файла устройства*/
#define DEVICE_FILE_NAME "/dev/my_char_dev"

#endif
     

