#include <dlfcn.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "operations.h"

int main(int argc, char **argv)
{
    int action = 0;
    bool flag = false;  //для выхода из цикла while(1) через case(action=3)
    char menu[50] = "1-pow(3); 2-pow(4); 3-quit.\n"; //меню выбора действий
    double number;
    double result;
    

        while(1)
    {
        printf("Select operation:\n");
        printf("%s",menu);
        while(scanf("%d", &action) == 0) //на случай, если введена не цифра
        {
            printf("Enter number! try again\n");
            while(getchar() != '\n');
        }
        if(action == 1 || action == 2) //нужен ли ввод ввод чисел
        {
            printf("Enter number\n");
            scanf("%lf", &number);
        }
        
        switch(action)
        {
        case 1:
            cube(number, &result);
            printf("Result: %0.2lf\n",result);
            break;
        case 2:
            fourth_degree(number, &result);
            printf("Result: %0.2lf\n",result);
            break;        
        case 3:
            flag = true;
            printf("Exit\n");
            break;
        default:
            printf("Wrong action\n");
            break;
        }
        if(flag == true) //выход из бесконечного цикла
        {
            break;
        }
    }
    return 0;
}

