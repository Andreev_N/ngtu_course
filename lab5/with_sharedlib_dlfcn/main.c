#include <dlfcn.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    int action = 0;
    bool flag = false;  //для выхода из цикла while(1) через case(action=5)
    char menu[50] = ""; //меню выбора действий
    double number;
    double result;

    /*Указатель на адрес подключаемой библиотеки*/
    void* ptr_library = NULL;
    /*Указатели на функции*/
    void (*cube_ptr)(double, double*) = NULL;
    void (*fourth_degree_ptr)(double, double*) = NULL;   

    ptr_library = dlopen("liboperations.so", RTLD_LAZY);

    if(!ptr_library)
    {
        fprintf(stderr, "dlopen() error: %s\n", dlerror());
        exit(1);
    }     

    cube_ptr = dlsym(ptr_library, "cube");
    fourth_degree_ptr = dlsym(ptr_library, "fourth_degree");

    /*Формирование меню в зависимости от подключенных функций*/
    if(cube_ptr)
        strcat(menu, "1-cube;");
    if(fourth_degree_ptr)
        strcat(menu, "2-fourth_degree;");
    strcat(menu, "3-quit.\n");

    while(1)
    {
        printf("Select operation:\n");
        printf("%s", menu);
        while(scanf("%d", &action) == 0) //на случай, если введена не цифра
        {
            printf("Enter number! try again\n");
            while(getchar() != '\n')
                ;
        }
        if((action == 1 && cube_ptr != 0) || (action == 2 && fourth_degree_ptr != 0)) //нужен ли ввод ввод чисел
        {
            printf("Enter number\n");
            scanf("%lf", &number);
        }

        switch(action)
        {
        case 1:
            if(cube_ptr)
            {
                cube_ptr(number, &result);
                printf("Result: %0.2lf\n", result);
            }
            else
                printf("unavailable function\n");
            break;
        case 2:
            if(fourth_degree_ptr)
            {
                fourth_degree_ptr(number, &result);
                printf("Result: %0.2lf\n", result);
            }
            else
                printf("unavailable function\n");
            break;
        case 3:
            flag = true;
            printf("Exit\n");
            break;
        default:
            printf("Wrong action\n");
            break;
        }
        if(flag == true) //выход из бесконечного цикла
        {
            if(ptr_library)
                dlclose(ptr_library); //удаление библиотек из памяти
            break;
        }
    }
    return 0;
}
