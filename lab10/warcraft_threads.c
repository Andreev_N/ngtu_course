#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#define MAXWORKERS 20
#define MAXGOLD 1000
#define RANDOM_TIME 5
#define GOLD_IN_ONE_WAY 10

#define min(a, b) ((a) < (b) ? (a) : (b))

struct
{
    pthread_mutex_t mutex;
    int max_gold;
} mutex_and_gold = { PTHREAD_MUTEX_INITIALIZER };

void* worker(void* worker_number)
{
    int* worker_gold = (int*)malloc(sizeof(int));
    *worker_gold = 0; //добытое золото этим рабочим(потоком)

    srand(getpid());

    while(1)
    {
        sleep(RANDOM_TIME + rand() % RANDOM_TIME);
        
        pthread_mutex_lock(&mutex_and_gold.mutex);
        
        if(mutex_and_gold.max_gold > 0)
        {
            mutex_and_gold.max_gold -= GOLD_IN_ONE_WAY;
            printf("Worker #%d take 10 gold\n", *((int*)worker_number));
            printf("Gold in mine %d\n", mutex_and_gold.max_gold);
            
            pthread_mutex_unlock(&mutex_and_gold.mutex);
            
            *worker_gold += GOLD_IN_ONE_WAY;
        }
        else
        {
            pthread_mutex_unlock(&mutex_and_gold.mutex);
            pthread_exit((void*)worker_gold);
        }
    }
}

int main(int argc, char* argv[], char* env[])
{
    int count_workers;  //число рабочих
    pthread_t* threads; //массив идентификаторов потока
    int result;         //результат создания и присоединения потоков
    int* worker_num;    //массив номеров рабочих
    void** gold_farm;   //возвращаемое значение из потока(добытое им золото)

    printf("Enter max gold in mine\n");
    scanf("%d", &mutex_and_gold.max_gold);
    printf("Enter number of workers\n");
    scanf("%d", &count_workers);
    printf("Work is started! Gold in mine:%d\n", mutex_and_gold.max_gold);

    /*ограничиваем количество потоков и рабочих*/
    count_workers = min(count_workers, MAXWORKERS);
    mutex_and_gold.max_gold = min(mutex_and_gold.max_gold, MAXGOLD);

    /*выделение памяти под массивы*/
    threads = (pthread_t*)malloc(sizeof(pthread_t) * count_workers);
    worker_num = (int*)malloc(sizeof(int) * count_workers);
    gold_farm = (void*)malloc(sizeof(void*) * count_workers);
    
    for(int i = 0; i < count_workers; i++)
    {
        worker_num[i] = i; //в поток будет передаваться номер рабочего параметром
        result = pthread_create(&threads[i], NULL, worker, &worker_num[i]);
        if(result != 0)
        {
            perror("Creating the first thread");
            return EXIT_FAILURE;
        }
    }
    
    for(int i = 0; i < count_workers; i++)
    {
        result = pthread_join(threads[i], &gold_farm[i]);
        if(result != 0)
        {
            perror("Joining the first thread");
            return EXIT_FAILURE;
        }
        else
        {
            printf("Worker [%d] extract %d gold\n", i, *((int*)gold_farm[i]));
        }
        free(gold_farm[i]);
    }
    
    if(mutex_and_gold.max_gold <= 0)
    {
        printf("Mine is EMPTY!\n");
    }

    free(gold_farm);
    free(worker_num);
    free(threads);

    return 0;
}
