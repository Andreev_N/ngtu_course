Настройка OSPF  

В качестве NAT реализован PAT  
Белый IP-адрес сервера: 222.234.20.2/24  
------------------------------------------------------------
![SSH](https://bitbucket.org/Andreev_N/ngtu_course/raw/master/Network_labs/Images/OSPF.png)

#Настройка коммутаторов, сабинтерфейсов маршрутизатора аналогична предыдущим работам => она не описывается, а сразу демонстрируется настройка OSPF

Router>enable - переходим в расширенный режим  
Router#configure terminal - переходим в режим конфигурации  

#Настройка loopback интерфейса на коммутаторе Router 0
Router0(config)#int loopback 0  
Router0(config-if)#ip address 192.168.100.1 255.255.255.255  
Router0(config-if)#no shutdown   
Router0(config-if)#exit  

#Настройка OSPF на коммутаторе Router 0
Router0(config)#router ospf 1		  			- единица номер процесса  
Router0(config-router)#network 192.168.2.0 0.0.0.255 area 0	- обозначаем сети для ospf с wildcard маской  
Router0(config-router)#network 192.168.5.0 0.0.0.255 area 0  
Router0(config-router)#network 10.10.10.0 0.0.0.3 area 0  
Router0(config-router)#exit  

#Настройка OSPF на коммутаторе Router 1
Router1(config)#int loopback 0  
Router1(config-if)#ip address 192.168.100.2 255.255.255.255  
Router1(config-if)#no shutdown   
Router1(config-if)#exit  
Router1(config)#router ospf 1  
Router1(config-router)#network 192.168.3.0 0.0.0.255 area 0  
Router1(config-router)#network 20.20.20.0 0.0.0.3 area 0  
Router1(config-router)#exit  
 
#Настройка OSPF на коммутаторе Router 2
Router2(config)#int loopback 0  
Router2(config-if)#ip address 192.168.100.3 255.255.255.255  
Router2(config-if)#no shutdown   
Router2(config-if)#exit  
Router2(config)#router ospf 1  
Router2(config-router)#network 192.168.4.0 0.0.0.255 area 0  
Router2(config-router)#network 192.168.6.0 0.0.0.255 area 0  
Router2(config-router)#network 30.30.30.0 0.0.0.3 area 0  
Router2(config-router)#exit  


#Настройка OSPF на коммутаторе Router 3
Router3(config)#int loopback 0  
Router3(config-if)#ip address 192.168.100.4 255.255.255.255  
Router3(config-if)#no shutdown   
Router3(config-if)#exit  
Router3(config)#ip route 0.0.0.0 0.0.0.0 222.234.100.1 		- прописываем дефолтный маршрут до роутера провайдера  
Router3(config)#router ospf 1  
Router3(config-router)#network 10.10.10.0 0.0.0.3 area 0  
Router3(config-router)#network 20.20.20.0 0.0.0.3 area 0  
Router3(config-router)#network 30.30.30.0 0.0.0.3 area 0  
Router3(config-router)#default-information originate		- распространяем заполненный дефолтный маршрут соседям  
Router3(config-router)#exit  

#Настройка NAT (PAT) на коммутаторе Router 3
Router3(config)#int fastEthernet 0/0  
Router3(config-if)#ip nat inside   
Router3(config-if)#exit  
Router3(config)#int fastEthernet 0/1  
Router3(config-if)#ip nat inside  
Router3(config-if)#exit  
Router3(config)#int fastEthernet 1/0  
Router3(config-if)#ip nat inside   
Router3(config-if)#exit  
Router3(config)#int fastEthernet 1/1  
Router3(config-if)#ip nat outside   
Router3(config-if)#exit  

Router3(config)#ip access-list standard MY_LIST  
Router3(config-std-nacl)#permit 192.168.2.0 0.0.0.255  
Router3(config-std-nacl)#permit 192.168.3.0 0.0.0.255  
Router3(config-std-nacl)#permit 192.168.4.0 0.0.0.255  
Router3(config-std-nacl)#permit 192.168.5.0 0.0.0.255  
Router3(config-std-nacl)#permit 192.168.6.0 0.0.0.255  
Router3(config-std-nacl)#exit  

Router3(config)#ip nat inside source list MY_LIST interface fa1/1 overload  


#Проверка NAT после ping сервера в интернете с локального ПК:
Router3#show ip nat translations   
Pro  Inside global     Inside local       Outside local      Outside global  
icmp 222.234.100.10:5  192.168.2.2:5      222.234.20.2:5     222.234.20.2:5  
icmp 222.234.100.10:6  192.168.2.2:6      222.234.20.2:6     222.234.20.2:6  
icmp 222.234.100.10:7  192.168.2.2:7      222.234.20.2:7     222.234.20.2:7  
icmp 222.234.100.10:8  192.168.2.2:8      222.234.20.2:8     222.234.20.2:8  

#Проверка маршрутизации маршрутизатора 3:
Router3#show ip route   
     10.0.0.0/30 is subnetted, 1 subnets  
C       10.10.10.0 is directly connected, FastEthernet0/0  
     20.0.0.0/30 is subnetted, 1 subnets  
C       20.20.20.0 is directly connected, FastEthernet0/1  
     30.0.0.0/30 is subnetted, 1 subnets  
C       30.30.30.0 is directly connected, FastEthernet1/0  
O    192.168.2.0/24 [110/2] via 10.10.10.1, 00:27:49, FastEthernet0/0  
O    192.168.3.0/24 [110/2] via 20.20.20.1, 00:27:39, FastEthernet0/1  
O    192.168.4.0/24 [110/2] via 30.30.30.1, 00:25:44, FastEthernet1/0  
O    192.168.5.0/24 [110/2] via 10.10.10.1, 00:27:49, FastEthernet0/0  
O    192.168.6.0/24 [110/2] via 30.30.30.1, 00:25:44, FastEthernet1/0  
     192.168.100.0/32 is subnetted, 1 subnets  
C       192.168.100.4 is directly connected, Loopback0  
C    222.234.100.0/24 is directly connected, FastEthernet1/1  
S*   0.0.0.0/0 [1/0] via 222.234.100.1  

