#Настройка BGP_OSPF_RIP_STATIC  

------------------------------------------------------------
![SSH](https://bitbucket.org/Andreev_N/ngtu_course/raw/master/Network_labs/Images/BGP_OSPF_RIP_STATIC.png)

#Настройка RIP на маршрутизаторе 10 (аналогичная настройка на 8,9,11)

Router10>enable  
Router10#configure terminal  
Router10(config)#route rip  
Router10(config-if)#version 2  
Router10(config-if)#network 100.100.100.0   
Router10(config-if)#network 110.110.110.0   
Router10(config-if)#exit  

#Настройка OSPF на роутере 0 (аналогично на 1,2,3)
Router0(config)#int loopback 0  
Router0(config-if)#ip address 192.168.100.2 255.255.255.255  
Router0(config-if)#no shutdown   
Router0(config-if)#exit  
Router0(config)#router ospf 1  
Router0(config-router)#network 10.10.10.0 0.0.0.3 area 0  
Router0(config-router)#network 20.20.20.0 0.0.0.3 area 0  
Router0(config-router)#exit  

#Настройка Static маршрутизации на роутере 5 (аналогично на 4,6,7)
Router5(config)#ip route 60.60.60.0 255.255.255.252 50.50.50.1   
Router5(config)#ip route 80.80.80.0 255.255.255.252 70.70.70.2  
Router5(config)#ip route 172.16.0.0 255.255.0.0 50.50.50.1  
Router5(config)#ip route 0.0.0.0 0.0.0.0 70.70.70.2  
 
#Настройка BGP на роутере 14 (аналогично на 1,11,12,13)
Router14(config)#route bgp6  
Router14(config-router)#network 150.150.150.0 mask 255.255.255.252  
Router14(config-router)#network 140.140.140.0 mask 255.255.255.252  
Router14(config-router)#network 180.180.180.0 mask 255.255.255.252  
Router14(config-router)#neighbor 140.140.140.1 remote-as 5  
Router14(config-router)#neighbor 150.150.150.1 remote-as 4  
Router14(config-router)#neighbor 180.180.180.1 remote-as 2  
Router14(config-router)#exit  


#На 11 роутере для RIP включаем  default-information originate и redistribute connected  
#На 1 роутере для OSPF включаем  default-information originate и redistribute bgp 1 subnets; для bgp 1 включаем redistribute ospf 1     


