Агрегирование портов
------------------------------------------------------------

![SSH](https://bitbucket.org/Andreev_N/ngtu_course/raw/master/Network_labs/Images/Aggregation.png)

#Статическое агрегирование между 2я коммутоторами  

#Настройка на Switch0
Switch>en  
Switch#conf t  
Switch(config)#int range fa0/1-2  
Switch(config-if-range)#channel-group 1 mode ?		-смотрим доступные режимы  
  active     Enable LACP unconditionally  
  auto       Enable PAgP only if a PAgP device is detected  
  desirable  Enable PAgP unconditionally  
  on         Enable Etherchannel only  
  passive    Enable LACP only if a LACP device is detected  
Switch(config-if-range)#channel-group 1 mode on  
Creating a port-channel interface Port-channel 1  
 
%LINK-5-CHANGED: Interface Port-channel 1, changed state to up  

%LINEPROTO-5-UPDOWN: Line protocol on Interface Port-channel 1, changed state to up  

Switch(config-if-range)#ex  
Switch(config)#ex  

#Аналогично на switch1
Switch>en  
Switch#conf t  
Switch(config)#int range fa0/1-2  
Switch(config-if-range)#channel-group 1 mode on  

#Просмотр информации о etherchannel  
Switch#show etherchannel summary   
Flags:  D - down        P - in port-channel  
        I - stand-alone s - suspended  
        H - Hot-standby (LACP only)  
        R - Layer3      S - Layer2  
        U - in use      f - failed to allocate aggregator  
        u - unsuitable for bundling  
        w - waiting to be aggregated  
        d - default port  

Number of channel-groups in use: 1  
Number of aggregators:           1  

Group  Port-channel  Protocol    Ports  
------+-------------+-----------+----------------------------------------------  

1      Po1(SU)           -      Fa0/1(P) Fa0/2(P)  





-----------------------------------------------
#Динамическое агрегирование LACP для трех коммутоторов 2го уровня и одного коммутатора 3го уровня
-----------------------------------------------
Switch>  
Switch>en  
Switch#conf t  
Switch(config)#int range fa 0/1-2  
Switch(config-if-range)#channel-protocol ?  
  lacp  Prepare interface for LACP protocol  
  pagp  Prepare interface for PAgP protocol  
Switch(config-if-range)#channel-protocol lacp   
Switch(config-if-range)#channel-group 1 mode active   
Creating a port-channel interface Port-channel 1  
Switch(config-if-range)#ex  
Switch(config)#int range fa 0/3-4  
Switch(config-if-range)#channel-protocol lacp   
Switch(config-if-range)#channel-group 2 mode active   
Switch(config-if-range)#  
Creating a port-channel interface Port-channel 2  
Switch(config-if-range)#ex  
Switch(config)#int range fa 0/5-6  
Switch(config-if-range)#channel-protocol lacp   
Switch(config-if-range)#channel-group 3 mode active   
Switch(config-if-range)#  
Creating a port-channel interface Port-channel 3  
Switch(config-if-range)#ex  
Switch(config)#ex  
Switch#show etherchannel summary   
Flags:  D - down        P - in port-channel  
        I - stand-alone s - suspended  
        H - Hot-standby (LACP only)  
        R - Layer3      S - Layer2  
        U - in use      f - failed to allocate aggregator  
        u - unsuitable for bundling  
        w - waiting to be aggregated  
        d - default port  


Number of channel-groups in use: 3  
Number of aggregators:           3  

Group  Port-channel  Protocol    Ports  
------+-------------+-----------+----------------------------------------------  

1      Po1(SU)           LACP   Fa0/1(P) Fa0/2(P)   
2      Po2(SU)           LACP   Fa0/3(P) Fa0/4(P)   
3      Po3(SU)           LACP   Fa0/5(P) Fa0/6(P)   

#На коммутаторе доступа Switch2
Switch>en  
Switch#conf t  
Switch(config)#int range fa 0/1-2  
Switch(config-if-range)#channel-protocol lacp  
Switch(config-if-range)#channel-group 1 mode passive   
Creating a port-channel interface Port-channel 1  
Switch(config-if-range)#ex  

#На коммутаторах Switch3 Switch4 настройки аналогичны
