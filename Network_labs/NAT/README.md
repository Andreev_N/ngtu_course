Настройка динамического NAT  

Имеется 3 сегмента сети с разными vlan:  
192.168.2.0/24  
192.168.3.0/24  
192.168.4.0/24  
Выделенный Pool адресов провайдером для NAT: 222.234.10.50 - 222.234.10.100 255.255.255.0  
Белый IP-адрес сервера: 222.234.20.2/24  
------------------------------------------------------------
![SSH](https://bitbucket.org/Andreev_N/ngtu_course/raw/master/Network_labs/Images/NAT.png)

NAT_GW>enable - переходим в расширенный режим  
NAT_GW#configure terminal - переходим в режим конфигурации  
NAT_GW(config)#interface fa0/1 - настройка интерфейса в сторону частной сети  
NAT_GW(config-if)#no shutdown - включаем интерфейс физически  
NAT_GW(config-if)#exit  

#Создаем саб-интерфейсы для соответствующих vlan
NAT-GW(config)#int fastEthernet 0/1.2  
NAT-GW(config-subif)#encapsulation dot1Q 2  
NAT_GW(config-subif)#ip address 192.168.2.1 255.255.255.0  
NAT_GW(config-subif)#ip nat inside - настраиваем интерфейс как внутренний  
NAT_GW(config-subif)#no shutdown   
NAT_GW(config-subif)#exit  

NAT-GW(config)#int fastEthernet 0/1.3  
NAT-GW(config-subif)#encapsulation dot1Q 3  
NAT_GW(config-subif)#ip address 192.168.3.1 255.255.255.0  
NAT_GW(config-subif)#ip nat inside  
NAT_GW(config-subif)#no shutdown   
NAT_GW(config-subif)#exit  

NAT-GW(config)#int fastEthernet 0/1.4  
NAT-GW(config-subif)#encapsulation dot1Q 4  
NAT_GW(config-subif)#ip address 192.168.4.1 255.255.255.0  
NAT_GW(config-subif)#ip nat inside   
NAT_GW(config-subif)#no shutdown   
NAT_GW(config-subif)#exit  

NAT_GW(config)#interface fa0/0 - настройки интерфейса в сторону провайдера  
NAT_GW(config-if)#ip address 222.234.10.2 255.255.255.0 - задаем Ip и маску  
NAT_GW(config-if)#no shutdown - включаем интерфейс физически  
NAT_GW(config-if)#ip nat outside - настраиваем интерфейс как внешний  
NAT_GW(config-if)#exit  

#Создание access листа
NAT-GW(config)#ip access-list standard MY_LIST  
NAT-GW(config-std-nacl)#permit 192.168.2.0 0.0.0.255  
NAT-GW(config-std-nacl)#permit 192.168.3.0 0.0.0.255  
NAT-GW(config-std-nacl)#permit 192.168.4.0 0.0.0.255  

#Создание динамического пула адресов и включение динамической трансляции
NAT_GW(config)#ip nat pool MY_POOL 222.234.10.50 222.234.10.100 netmask 255.255.255.0  
NAT_GW(config)#ip nat inside source list MY_LIST pool MY_POOL  

#Статический маршрут в сторону провайдера
NAT_GW(config)#ip route 0.0.0.0 0.0.0.0 100.0.0.254  

#После обращения из локального хоста на сервер с белым ip адресом (по HTTP):
NAT-GW#show ip nat translations   
Pro  Inside global     Inside local       Outside local      Outside global  
tcp 222.234.10.50:1025 192.168.2.2:1025   222.234.20.2:80    222.234.20.2:80  
tcp 222.234.10.50:1026 192.168.2.2:1026   222.234.20.2:80    222.234.20.2:80  
tcp 222.234.10.50:1027 192.168.2.2:1027   222.234.20.2:80    222.234.20.2:80  
tcp 222.234.10.50:1028 192.168.2.2:1028   222.234.20.2:80    222.234.20.2:80  
tcp 222.234.10.50:1029 192.168.2.2:1029   222.234.20.2:80    222.234.20.2:80  
tcp 222.234.10.50:1030 192.168.2.2:1030   222.234.20.2:80    222.234.20.2:80  

