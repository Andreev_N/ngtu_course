Настройте работу протоколов STP, RSTP, MSTP. Снова соедините два соседних порта
кабелем, снова исследуйте состояние коммутатора.
------------------------------------------------------------
![SSH](https://bitbucket.org/Andreev_N/ngtu_course/raw/master/Network_labs/Images/rstp.png)

1)STP  

#Изменение приоритета для назначения коммутатора CORE корневым для всех vlan
CORE#conf t
CORE(config)#spanning-tree vlan 10 root primary  
CORE(config)#spanning-tree vlan 20 root primary   
SCORE(config)#spanning-tree vlan 30 root primary   
CORE(config)#exit


#Просмотр конфигурации STP на корневом коммутаторе
CORE#show spanning-tree   
VLAN0010  
  Spanning tree enabled protocol ieee  
  Root ID    Priority    24586  
             Address     000D.BD07.9AD0  
             This bridge is the root  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
  
  Bridge ID  Priority    24586  (priority 24576 sys-id-ext 10)  
             Address     000D.BD07.9AD0  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
             Aging Time  20  

Interface        Role Sts Cost      Prio.Nbr Type  
---------------- ---- --- --------- -------- --------------------------------  
Fa0/1            Desg FWD 19        128.1    P2p  
Fa0/2            Desg FWD 19        128.2    P2p  
Fa0/3            Desg FWD 19        128.3    P2p  
Fa0/4            Desg FWD 19        128.4    P2p  

VLAN0020  
  Spanning tree enabled protocol ieee  
  Root ID    Priority    24596  
             Address     000D.BD07.9AD0  
             This bridge is the root  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
  
  Bridge ID  Priority    24596  (priority 24576 sys-id-ext 20)  
             Address     000D.BD07.9AD0  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
             Aging Time  20  

Interface        Role Sts Cost      Prio.Nbr Type  
---------------- ---- --- --------- -------- --------------------------------  
Fa0/1            Desg FWD 19        128.1    P2p  
Fa0/2            Desg FWD 19        128.2    P2p  
Fa0/3            Desg FWD 19        128.3    P2p  
Fa0/4            Desg FWD 19        128.4    P2p  

VLAN0030  
  Spanning tree enabled protocol ieee  
  Root ID    Priority    24606  
             Address     000D.BD07.9AD0  
             This bridge is the root  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
  
  Bridge ID  Priority    24606  (priority 24576 sys-id-ext 30)  
             Address     000D.BD07.9AD0  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
             Aging Time  20  

Interface        Role Sts Cost      Prio.Nbr Type  
---------------- ---- --- --------- -------- --------------------------------  
Fa0/1            Desg FWD 19        128.1    P2p  
Fa0/2            Desg FWD 19        128.2    P2p  
Fa0/3            Desg FWD 19        128.3    P2p  
Fa0/4            Desg FWD 19        128.4    P2p  

#Просмотр конфигурации STP на коммутаторе switch3  
VLAN0010  
  Spanning tree enabled protocol ieee  
  Root ID    Priority    24586  
             Address     000D.BD07.9AD0  
             Cost        19  
             Port        4(FastEthernet0/4)  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
  
  Bridge ID  Priority    32778  (priority 32768 sys-id-ext 10)  
             Address     00E0.B031.3389  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
             Aging Time  20  

Interface        Role Sts Cost      Prio.Nbr Type  
---------------- ---- --- --------- -------- --------------------------------  
Fa0/3            Altn BLK 19        128.3    P2p  
Fa0/4            Root FWD 19        128.4    P2p  
Fa0/2            Altn BLK 19        128.2    P2p  
    
VLAN0020   
  Spanning tree enabled protocol ieee  
  Root ID    Priority    24596  
             Address     000D.BD07.9AD0  
             Cost        19  
             Port        4(FastEthernet0/4)  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  

  Bridge ID  Priority    32788  (priority 32768 sys-id-ext 20)  
             Address     00E0.B031.3389  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
             Aging Time  20  

Interface        Role Sts Cost      Prio.Nbr Type  
---------------- ---- --- --------- -------- --------------------------------  
Fa0/3            Altn BLK 19        128.3    P2p  
Fa0/4            Root FWD 19        128.4    P2p  
Fa0/1            Desg FWD 19        128.1    P2p  
Fa0/2            Altn BLK 19        128.2    P2p  

VLAN0030  
  Spanning tree enabled protocol ieee  
  Root ID    Priority    24606  
             Address     000D.BD07.9AD0  
             Cost        19  
             Port        4(FastEthernet0/4)  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
  
  Bridge ID  Priority    32798  (priority 32768 sys-id-ext 30)  
             Address     00E0.B031.3389  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
             Aging Time  20  

Interface        Role Sts Cost      Prio.Nbr Type  
---------------- ---- --- --------- -------- --------------------------------  
Fa0/3            Altn BLK 19        128.3    P2p  
Fa0/4            Root FWD 19        128.4    P2p  
Fa0/2            Altn BLK 19        128.2    P2p  

#Доступные команды для подробного просмотра конфигурации STP
show spanning-tree ?  
  active             Report on active interfaces only  
  detail             Detailed information  
  inconsistentports  Show inconsistent ports  
  interface          Spanning Tree interface status and configuration  
  summary            Summary of port states  
  vlan               VLAN Switch Spanning Trees  

#Кадр STP BPDU:

-Идентификатор версии протокола STA (2 байта). Коммутаторы должны поддерживать одну и ту же версию протокола STA.  
-Версия протокола STP (1 байт).  
-Тип BPDU (1 байт). Существует 2 типа BPDU — конфигурационный и уведомление о реконфигурации.  
-Флаги (1 байт).  
-Root ID — 32769 / 0009.7C94.0557. Это, как раз, та самая связка «приоритет + MAC-адрес». Но тут интересный момент. Если в классическом STP приоритет по-умолчанию равнялся 32768, то здесь мы видим 32769. В протоколе PVST+ и других, умеющих работать с VLAN, к приоритету добавляется параметр system ID extension. Этот параметр содержит в себе номер VLAN-а и по нему коммутатор понимает, к какому процессу STP его отнести. То есть в данном случае у нас VLAN №1 => приоритет = 32768 + 1 = 32769. Если бы мы настраивали для 10-ого VLAN-а, то приоритет равнялся 32778. Ну а после дробной черты, сам MAC-адрес интерфейса.  
-Root Path Cost — стоимость пути. Мы изучаем кадр, когда он выходит из корневого коммутатора => там стоит 0.  
-Bridge ID — идентификатор коммутатора, который ретранслирует данный BPDU. В данный момент он такой же, как и Root ID.  
-Port ID — идентификатор порта. Такой же, как и приоритет — 32769.  
-Message Age — интервал времени. Так как BPDU «свежий», то там стоит 0. Измеряется в единицах по 0,5 с, служит для выявления устаревших сообщений  
-Max Age — максимальное время жизни — 20 секунд.Если кадр BPDU имеет время жизни, превышающее максимальное, то кадр игнорируется коммутаторами.  
-Hello Time — интервал посылки приветствия — 2 секунды.
-Forward Delay — указывает сколько секунд находиться в одной фазе (прослушивания или обучения) — 15 секунд.  

Выбор на каком коммутатоторе заблокировать порт происходит по следующей схеме:  
    Меньшего Root Path Cost.  
    Меньшего Bridge ID.  
    Меньшего Port ID.  

В STP существуют следующие состояния портов:  

1.Blocking — блокирование. В данном состоянии через порт не передаются никакие фреймы. Используются для избежания избыточности топологии.  
2.Listening — прослушивание. Как мы говорили выше, что до того, пока еще не выбран корневой коммутатор, порты находятся в специальном состоянии, где передаются только BPDU, фреймы с данными не передаются и не принимаются в этом случае. Состояние Listening не переходит в следующее даже, если Root Bridge определен. Данное состояние порта длится в течении Forward delay timer, который, по умолчанию, равен 15.  
3.Learning — обучение. В данном состояние порт слушает и отправляет BPDU, но информацию с данными не отправляет. Отличие данного состояния от Listening в том, что фреймы с данными, который приходят на порт изучаются и информация о MAC-адресах заносится в таблицу MAC-адресов коммутатора. Переход в следующее состояние также занимает Forward delay timer.  
4.Forwarding — пересылка. Это обычное состояние порта, в котором отправляются и пакеты BPDU, и фреймы с обычными данными.   Таким образом, если мы пройдемся по схеме, когда коммутаторы только загрузились, то получается следующая схема:  
-Коммутатор переводит все свои подключенные порты в состояние Listening и начинает отправлять BPDU, где объявляет себя корневым коммутатором. В этот период времени, либо коммутатор остается корневым, если не получил лучший BPDU, либо выбирает корневой коммутатор. Это длится 15 секунд.  
-После переходит в состояние Learning и изучает MAC-адреса. 15 секунд.  
-Определяет какие порты перевести в состояние Forwarding, а какие в Blocking.  



2)RSTP.   
Если в классическом STP было 4 состояния (Blocking, Listening, Learning, Forwarding), то в RSTP их стало меньше. Всего 3 (Discarding, Listening и Forwarding). То есть коммутатор отбрасывает, изучает или пересылает.RSTP в первую очередь опирается на работу механизмов, не привязанных к стандартным таймерам.  Быструю сходимость протокол обеспечивает тем, что заранее просчитывает, какой порт включить, если откажет работающий. Тем самым, при отказе порта, он не начинает судорожно изучать топологию и прыгать по различным состояниям, а просто переключается на заранее просчитанный.   

#Включить протокол RSTP можно командой (прописываем на всех коммутаторах):  
CORE(config)#spanning-tree mode rapid-pvst  

CORE2#show spanning-tree   
VLAN0010  
  Spanning tree enabled protocol rstp  
  Root ID    Priority    24586  
             Address     000D.BD07.9AD0  
             This bridge is the root  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  

  Bridge ID  Priority    24586  (priority 24576 sys-id-ext 10)  
             Address     000D.BD07.9AD0  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
             Aging Time  20  

Interface        Role Sts Cost      Prio.Nbr Type  
---------------- ---- --- --------- -------- --------------------------------  
Fa0/2            Desg FWD 19        128.2    P2p  
Fa0/4            Desg FWD 19        128.4    P2p  
Fa0/1            Desg FWD 19        128.1    P2p  
Fa0/3            Desg FWD 19        128.3    P2p  

VLAN0020  
  Spanning tree enabled protocol rstp  
  Root ID    Priority    24596  
             Address     000D.BD07.9AD0  
             This bridge is the root  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
  
  Bridge ID  Priority    24596  (priority 24576 sys-id-ext 20)  
             Address     000D.BD07.9AD0  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
             Aging Time  20  

Interface        Role Sts Cost      Prio.Nbr Type  
---------------- ---- --- --------- -------- --------------------------------  
Fa0/2            Desg FWD 19        128.2    P2p  
Fa0/4            Desg FWD 19        128.4    P2p  
Fa0/1            Desg FWD 19        128.1    P2p  
Fa0/3            Desg FWD 19        128.3    P2p  

VLAN0030  
  Spanning tree enabled protocol rstp  
  Root ID    Priority    24606  
             Address     000D.BD07.9AD0  
             This bridge is the root  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
  
  Bridge ID  Priority    24606  (priority 24576 sys-id-ext 30)  
             Address     000D.BD07.9AD0  
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec  
             Aging Time  20  
 
Interface        Role Sts Cost      Prio.Nbr Type  
---------------- ---- --- --------- -------- --------------------------------  
Fa0/2            Desg FWD 19        128.2    P2p  
Fa0/4            Desg FWD 19        128.4    P2p  
Fa0/1            Desg FWD 19        128.1    P2p  
Fa0/3            Desg FWD 19        128.3    P2p  


3)MSTP.  
В дополнение  к  обеспечению  быстрой  сходимости  сети MSTP  он  позволяет настраивать  отдельное  связующее  дерево  для  любой  VLAN  или группы  VLAN,создавая  множество  маршрутов  передачи  трафика  и позволяя осуществлять балансировку нагрузки.   
Логическая структура MSTP: протокол MSTP делит коммутируемую сеть на регионы MST (Multiple  Spanning  Tree  (MST)  Region),  каждый  из  которых  может содержать  множество  копий  связующих  деревьев  (Multiple  Spanning Tree Instance, MSTI) с независимой друг от друга топологией. Другими словами,  регион  MST,  представляющий  собой  набор  физически подключенных друг к другу коммутаторов, делит данную физическую топологию на множество логических.
Для  того  чтобы  два  и  более  коммутатора  принадлежали одному   региону   MST,   они   должны   обладать   одинаковой конфигурацией MST. Конфигурация MST включает такие параметры, как  номер  ревизии  MSTP  (MSTP  revision  level  number),  имя  региона (Region  name),  карту  привязки  VLAN  к  копии  связующего  дерева (VLAN-to-instance mapping). 
Роли портов MSTP:  
•Корневой  порт  (Root  Port) – это  порт,  который  обладает минимальной стоимостью пути от коммутатора до корневого моста.
•Назначенный   порт   (Designated     Port) – это   порт, обладающий  наименьшей  стоимостью  пути  от  подключенного сегмента сети до корневого моста.  
•Альтернативный/резервный  порт  (Alternate/Backup  Port) – это порт, который обеспечивает подключение, если происходит потеря соединения с какими-либо коммутаторами или сегментами сети.  
•Мастер-порт (Master Port) – это порт, который обеспечивает подключение региона к корневому мосту, находящемуся за пределами данного региона.  
•Пограничный  порт  (Boundary  Port) – это  порт,  который подключает MST-регион к другому региону или SST-мост.  

#Настройка MSTP:

spanning-tree mst configuration  
name H2SO4			-Задаём имя региона  
revision 1			-Номер ревизии конфига должен обязательно совпадать во всем регионе  

Разбиваем наши вланы на 2 instance:  
instance 1 vlan 1-35,101,111-500,1001-4094  
instance 2 vlan 36-100,102-110,501-1000  
#Cмотрим, что у нас получилась за конфигурация:
show pending  
Pending MST configuration  
Name      [H2SO4]   
Revision  1     Instances configured 3  
  
Instance  Vlans mapped  
--------  ---------------------------------------------------------------------  
0         none  
1         1-35,101,111-500,1001-4094  
2         36-100,102-110,501-1000  

spanning-tree mode mst  
spanning-tree priority 28672  
exit  
spanning-tree mst 1 priority 28672  




