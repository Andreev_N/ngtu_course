Настроить VLAN на двух коммутаторах MES, соединить их тагированными портами и
проверить их работоспособность(компьютеры находящиеся в VLAN с одинаковыми
номерами должны видеть друг друга на разных MESах)
------------------------------------------------------------
![SSH](https://bitbucket.org/Andreev_N/ngtu_course/raw/master/Network_labs/Images/vlan.png)
  
Switch>enable - для перехода в расширенный режим  
Switch#configure terminal - для перехода в режим конфигурации  

#Создание vlan'ов 
Switch(config)#vlan 2  
Switch(config-vlan)#exit  

... - аналогично для vlan 3,4,5;

#Сопостовляем интерфейсы vlan'ам, устанавливаем режим портов - нетегированные (режим access); 
Switch(config)#interface fastEthernet 0/2  
Switch(config-if)#switchport mode access  
Switch(config-if)#switchport access vlan 2  
Switch(config-if)#exit  

... - аналогично для vlan 3,4,5;  

#Настройка тегированных портов между коммутаторами
Switch(config)#int gigabitEthernet 0/1  
Switch(config-if)#switchport mode trunk  
Switch(config-if)#switchport trunk allowed vlan 2-5  
Switch(config-if)#exit  

Switch#show vlan  

VLAN Name                             Status    Ports  
---- -------------------------------- --------- -------------------------------  
1    default                          active    Fa0/5, Fa0/6, Fa0/7, Fa0/8  
                                                Fa0/9, Fa0/10, Fa0/11, Fa0/12    
                                                Fa0/13, Fa0/14, Fa0/15, Fa0/16    
                                                Fa0/17, Fa0/18, Fa0/19, Fa0/20  
                                                Fa0/21, Fa0/22, Fa0/23, Fa0/24  
                                                Gig0/2  
2    VLAN0002                         active    Fa0/2  
3    VLAN0003                         active    Fa0/4  
4    VLAN0004                         active    Fa0/1  
5    VLAN0005                         active    Fa0/3  
1002 fddi-default                     active    
1003 token-ring-default               active    
1004 fddinet-default                  active    
1005 trnet-default                    active   

... - аналогичные действия на 2м switch;
