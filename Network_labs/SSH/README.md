Подключение консольным кабелем к коммутатору и настройка SSH
------------------------------------------------------------
Для управления сетевыми устройствами будет использоваться сеть 172.16.0.0/24 и VLAN 2

![SSH](https://bitbucket.org/Andreev_N/ngtu_course/raw/master/Network_labs/Images/SSH.png)

Switch>enable - для перехода в расширенный режим  
Switch#configure terminal - для перехода в режим конфигурации  

#Указываем домен и имя устройства (для генерации ключа); 
Switch(config)#ip domain name domain.local  
Switch(config)#hostname cisco-ssh  

#Настраиваем ip-адрес, маску для vlan 2, включаем интерфейс;
cisco-ssh(config)#interface vlan 2  
cisco-ssh(config-if)#ip address 172.16.0.150 255.255.255.0  
cisco-ssh(config-if)#no shutdown   
cisco-ssh(config-if)#exit  

#Добавляем в vlan 2 порт fastEthernet 0/1
cisco-ssh(config)#interface fastEthernet 0/1  
cisco-ssh(config-if)#switchport mode access  
cisco-ssh(config-if)#switchport access vlan 2  
cisco-ssh(config-if)#exit  

#Создаем пользователя user и защищаем переход в привелегированный режим вводом пароля
cisco-ssh(config)#username user secret password  
cisco-ssh(config)#enable secret password  

#Настройка SSH
cisco-ssh(config)#username user secret password		- заводим пользователя user с паролем password  
cisco-ssh(config)#enable secret password		- защищаем привилигированный режим  
cisco-ssh(config)#line vty 0 4				- входим в режим конфигурирования терминальный линий  
cisco-ssh(config-line)#login local			- авторизация по имени и паролю (из локальной базы)   
cisco-ssh(config-line)#transport input ssh		- разрешаем доступ только по SSH   
cisco-ssh(config-line)#exec-timeout 0 60		- при бездеятельности отключаем от консоли через 60 сек.   
cisco-ssh(config-line)#logging synchronous		- запрет вывода консольных сообщений, прерывающих ввод команд 								  в консольном режиме  
cisco-ssh(config-line)#exit  
cisco-ssh(config)#ip ssh version 2			- ставим 2ю версию SSH   
cisco-ssh(config)#crypto key generate rsa		- генерируем ключи (1024 kb)  

The name for the keys will be: cisco-ssh.domain.local  
How many bits in the modulus [512]: 1024  
% Generating 1024 bit RSA keys, keys will be non-exportable...[OK]  

#Подключение с ноутбука по SSH
ssh -l user 172.16.0.150


